<?php
/*if(!isset($_COOKIE['dev'])){
    include("maintain.php");die;
}
*/
// Test
if (isset($_GET['postlen'])) {

    function ___toMemorySize($size)
    {
        $unit = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        return @round($size/pow(1024,($i = floor(log($size,1024)))),2) . ' ' . (isset($unit[$i]) ? $unit[$i] : 'B');
    }

    echo '<pre>';

    if (isset($_POST) && count($_POST) > 0) {
        echo "OK\n";
        echo ___toMemorySize(strlen(serialize($_POST))) . PHP_EOL;
        print_r($_POST);
    }

    $loop = isset($_GET['postlen']) ? $_GET['postlen'] : 1;
    $m1 = memory_get_usage();
    $content = str_repeat(' ', $loop * 1024);
    $m2 = memory_get_usage();

    echo 'memory: ' . ___toMemorySize($m2 - $m1) . "\n";
    echo 'length: ' . strlen($content) . "\n";
    ?>
    <form action="" method="POST">
        <textarea name="content" cols="30" rows="10"><?php echo htmlspecialchars($content)?></textarea><br>
        <input type="submit" value="Submit" />
    </form>
    <?php
    die;
}

date_default_timezone_set("Asia/Ho_Chi_Minh");
define('MEMORY_USAGE', memory_get_usage());
define('REQUEST_TIME_START', microtime(true));

// Define base path
define('ROOT', realpath(dirname(__FILE__)));
define('LIBRARY_PATH', ROOT . DIRECTORY_SEPARATOR . '../library');
define('APPLICATION_PATH', ROOT . DIRECTORY_SEPARATOR . '../application');
define('UPLOAD_PATH', ROOT);


//define('APPLICATION_PATH', ROOT . DIRECTORY_SEPARATOR . 'application' . DIRECTORY_SEPARATOR . APPLICATION_HOST);

// Define application environment
defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Include path
set_include_path(implode(PATH_SEPARATOR, array(LIBRARY_PATH, get_include_path())));

//Add Google Library
define('GOOGLE_LIB', LIBRARY_PATH.'/Google');
//define('CREDENTIALS_PATH', GOOGLE_LIB.'/php-yt-oauth2.json');
define('CREDENTIALS_PATH', GOOGLE_LIB.'/php-yt-oauth2.json');
if (!file_exists($file =  GOOGLE_LIB. '/vendor/autoload.php')) {
    echo "please run composer require google/apiclient:~2.0";die;
}
require_once GOOGLE_LIB. '/vendor/autoload.php';
;

require 'App.php';
require 'Config.php';
require 'Zend/Application.php';


$application = new Zend_Application(APPLICATION_ENV);
$application->getAutoloader()->registerNamespace(array('My_', 'Utility_', 'Plugin_','Google_'));
$application->setOptions(require APPLICATION_PATH . '/configs/application.php')->bootstrap()->run();

// ini_set('max_execution_time', 30000); //300 seconds = 5 minutes
// My_Cronjob_Playlist::takecarePlaylist();