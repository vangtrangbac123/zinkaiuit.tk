/**
 * Created by Sandeep on 01/06/14.
 */
angular.module('cinema.controllers',[]).controller('index',function($scope,$state,popupService,$window,Cinema){

    //$scope.movies = Movie.query();
    $scope.cinemas = data['index'];
    $scope.deleteCinema = function(cinema){
        if(popupService.showPopup('Really delete this?')){
            cinema.$delete(function(){
                $window.location.href='';
            });
        }
    }

}).controller('detail',function($scope,$stateParams,Cinema){
    //var result = Cinema.get({id:$stateParams.id});
    $scope.cinema   = data['detail'];
    $scope.location = data['location'];
     $scope.updateCinema = function(){
        $scope.cinema._mt = 'Cinema.addCinema';
        Cinema.save($scope.cinema,function(){
            //$window.location.href='/';
        });
    };

}).controller('add',function($scope,$state,$stateParams,$window,Cinema){
    //$scope.cinema = new Cinema();
    $scope.addCinema=function(){
        $scope.cinema._mt = 'Cinema.addCinema';
        Cinema.save($scope.cinema,function(){
            //$window.location.href='/';
        });
    }

}).controller('edit',function($scope,$state,$stateParams,Cinema){

    $scope.updateCinema = function(){
        $scope.cinema._mt = 'Cinema.addCinema';
        Cinema.save($scope.cinema,function(){
            //$window.location.href='/';
        });
    };

    $scope.loadCinema = function(){
        $scope.cinema = Movie.get({id:$stateParams.id});
    };

    $scope.loadMovie();
});

//News
angular.module('news.controllers',[]).controller('index',function($scope,$state,popupService,$window,News){

    //$scope.movies = Movie.query();
    $scope.news = data['index']['rows'];
    $scope.currentPage = 0;
    $scope.count =  5;
    $scope.total =  data['index']['total']/$scope.count;
     $scope.range = function (start, end) {
        var ret = [];
        if (!end) {
            end = start;
            start = 0;
        }
        for (var i = start; i < end; i++) {
            ret.push(i);
        }
        return ret;
    };

    $scope.prevPage = function () {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    };

    $scope.nextPage = function () {
        if ($scope.currentPage < $scope.total - 1) {
            $scope.currentPage++;
        }
    };

    $scope.setPage = function () {
        $scope.currentPage = this.n;
        News.get({'mt':'News.getList','page':$scope.currentPage},function(result){
             $scope.news  =  result['result']['rows'];
             $scope.total =  result['result']['total']/$scope.count;
        });


    };

}).controller('edit',function($scope,$stateParams,News){
    //var result = Cinema.get({id:$stateParams.id});
    $scope.news      = data['detail'];
    $scope.p_cinema  = data['p_cinema'];
    $scope.cinema    = data['cinema'];
    $scope.film      = data['film'];
    $scope.list_film = [];
     $scope.updateNews = function(){
        $scope.news._mt = 'Cinema.addCinema';
        News.save($scope.news,function(){
            //$window.location.href='/';
        });
        $scope.list_film._mt = 'Cinema.addCinema';
        News.save($scope.list_film,function(){
            //$window.location.href='/';
        });
    };
    $scope.addListFilm = function () {
           var index = $scope.list_film.indexOf($scope.filmList);
            if (index == -1) {
              $scope.list_film.push($scope.filmList);
            }else{
                alert('Film Exist');
            }
    };
    $scope.deleteFilm = function (f) {
           var index = $scope.list_film.indexOf(f)
          $scope.list_film.splice(index, 1);
    };

}).controller('add',function($scope,$state,$stateParams,$window,News){
    //$scope.cinema = new Cinema();
    $scope.addNews=function(){
        $scope.cinema._mt = 'Cinema.addCinema';
        Cinema.save($scope.cinema,function(){
            //$window.location.href='/';
        });

    }

});