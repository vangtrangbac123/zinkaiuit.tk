var uploadCallback = [];
var uploadModal;
var uploadFrame;

function generateGallery(picUrls) {
    var arrImage = [];
    $(picUrls).each(function(){
        arrImage.push($(this).val());
    });
    var first = arrImage.length > 0 ? arrImage[0] : false;
    for (var i = 0; i < uploadCallback.length; i++) {
        uploadCallback[i].call(this, first, arrImage, picUrls);
    }
}

function showUploadModal() {
    uploadModal.fadeIn();
}

function hideUploadModal() {
    uploadModal.fadeOut(function() {
        uploadFrame.attr('src', uploadFrame.data('src'));
    });
}



$(function() {

    var body = $('body');
    var controller = body.data('controller');
    var action = body.data('action');
    $('#menu > ul > li').each(function() {
        var it = $(this);
        if (it.data('controller') == controller) {
            it.addClass('nav-expanded nav-active');
            it.find('ul > li').each(function() {
                var item = $(this);
                if (item.data('action') == action) {
                    item.addClass('nav-active');
                }
            });
        }
    });

    uploadModal = $('#modal-upload');
    uploadFrame = $('#iframe-upload');

    // image uploader
    (function() {

        var img = $('img.img-uploader');
        if (img.length == 0) return;

        var currentImg;
        uploadCallback.push(function(url) {
            hideUploadModal();
            if (url) {
                var params = currentImg.data();
                params.url = url;
                currentImg.attr('src', url);
                $.post('/admin/ajax/image/', params, function(data) {

                });
            }
        });

        img.click(function() {
            currentImg = $(this);
            showUploadModal();
        });

    })();

        // image uploader
    (function() {

        var btn_start = $('.btn_start');
        if (btn_start.length == 0) return;



        btn_start.click(function(){
            parent = $(this).parent().parent();
            btn_now = $(this);
            is_start = parent.data('start');
             var params = {
                'user_id':     parent.data('userid'),
                'youtube_id':  parent.data('youtube'),
                'is_start':    parent.data('start')
            }

            $.post('/admin/ajax/addyoutube/', params, function(data) {
                if(data){
                    btn_now.toggleClass('btn_active');
                }
            });

        });



    })();

    // upload image to gallery
    (function() {
        var gallery = $('#justified-gallery');
        if (gallery.length == 0) return;

        gallery.justifiedGallery({
            'sizeRangeSuffixes': {
                'lt100': '',
                'lt240': '',
                'lt320': '',
                'lt500': '',
                'lt640': '',
                'lt1024': ''
            }
        });

        uploadCallback.push(function(url, urls) {
            hideUploadModal();
            if (urls.length > 0) {
                $.post('/admin/ajax/gallery/', {
                    'product_id': pId,
                    'type_id': 2,
                    'urls': urls
                }, function(data) {
                    window.location.reload();
                });
            }
        });

        // add
        $('#add-image').click(showUploadModal);

        gallery.find('.delete').click(function() {
            if (confirm('Remove?')) {
                var it = $(this);
                var p = it.parent();
                $.post('/admin/ajax/gallery-remove/', {
                    'product_id': pId,
                    'type_id': 2,
                    'image_id': it.data('id')
                }, function(data) {
                    p.fadeOut(function() {
                        p.remove();
                    });
                });
            }
        });

    })();

    (function(){
        var body = $('body');
        var controller = body.data('controller');
        if(controller == 'gallery'){
            uploadCallback.push(function(url, urls) {
                hideUploadModal();
                if (urls.length > 0) {
                    $.post('/admin/ajax/gallerycamera/', {
                        'gallery_id': galleryId,
                        'type_id': 1,
                        'urls': urls
                    }, function(data) {
                        window.location.reload();
                    });
                }
            });

            // add
            $('#add-image').click(showUploadModal);

            var gallery = $('#gallery-camera');
            gallery.find('.delete').click(function() {
                if (confirm('Remove?')) {
                    var it = $(this);
                    var p = it.parent();
                    $.post('/admin/ajax/gallery-camera-remove/', {
                        'gallery_id': galleryId,
                        'type_id': 1,
                        'image_id': it.data('id')
                    }, function(data) {
                        p.fadeOut(function() {
                            p.remove();
                        });
                    });
                }
            });
        }
    })();

    (function() {
        var artist = $('#actor_artist_id');

        if (artist.length == 0) return;

        var filmId = $('input[name="film_id"]').val();
        var type = $('#actor_type_id');
        var charname = $('#actor_charname');
        var number = $('#actor_number');
        var btn = $('#actor_add');
        var list = $('#list-actor');

        function getValue() {
            return {
                film_id: filmId,
                artist_id: $.trim(artist.val()),
                type_id: $.trim(type.val()),
                char_name: $.trim(charname.val()),
                number: $.trim(number.val())
            };
        }

        function getHtml(data) {
            var html = '';
            html += '<li data-id="'+data.artist_id+'">';
            html += '<img src="'+data.path+'" width="50" height="50" />';
            html += '<span class="artist">'+data.artist_name+'</span>';
            html += '<span class="delete fa fa-times"></span>';
            html += '<div class="character">'+data.char_name+'</div>';
            html += '</li>';
            return html;
        }

        function notification(text) {
            return new PNotify({
                title: 'Error',
                text: text,
                type: 'error',
                buttons: {
                    sticker: false
                }
            });
        }

        function success(text) {
            return new PNotify({
                title: 'Success',
                text: text,
                type: 'success',
                buttons: {
                    sticker: false
                }
            });
        }

        function reset() {
            artist.select2('val', '');
            charname.val('');
            number.val('');
        }

        artist.select2({
            minimumInputLength: 2,
            ajax: {
                url: '/admin/ajax/artist/',
                quietMillis: 300,
                data: function(term, page) {
                    return {
                        s: term
                    };
                },
                results: function(data, page) {
                    return {results: data};
                }
            }
        });

        $(document).on('click', '#list-actor .delete', function() {
            var li = $(this).parent();
            var id = li.data('id');
            li.css('opacity', 0.5);
            $.post('/admin/ajax/delete/', {
                type: 'actor',
                film_id: filmId,
                artist_id: id
            }, function(result) {
                li.remove();
                success('Remove successfully.');
            }).fail(function() {
                notification('Remove fail. Please try again.');
            });
        });



        btn.click(function() {
            var data = getValue();
            if (data.artist_id == '') {
                notification('Please choose an Artist.');
            } else if (data.charname == '') {
                notification('Please input character name.');
            } else {
                $.post('/admin/ajax/add-actor/', data, function(result) {
                    if (result) {
                        list.append(getHtml(result));
                        reset();
                    } else {
                        notification('Add fail. Please try again.');
                    }
                }).fail(function() {
                    notification('Add fail. Please try again.');
                });
            }
        });


    })();

    (function() {
        function error(text) {
            return new PNotify({
                title: 'Error',
                text: text,
                type: 'error',
                buttons: {
                    sticker: false
                }
            });
        }

        function success(text) {
            return new PNotify({
                title: 'Success',
                text: text,
                type: 'success',
                buttons: {
                    sticker: false
                }
            });
        }

        $('table .delete-row').click(function(e) {
            e.preventDefault();
            var tr = $(this).parents('tr');
            var id = tr.data('id');
            var type = tr.data('type');
            if (tr.hasClass('loading')) return;
            if (confirm('Remove?')) {
                tr.addClass('loading');
                $.post('/admin/ajax/delete/', {
                    id: id,
                    type: type
                }, function(result) {
                    tr.removeClass('loading');
                    if (result) {
                        tr.fadeOut(function() {
                            tr.remove();
                        });
                        success('Remove successfully.');
                    } else {
                        error('Remove fail. Please try again.');
                    }
                }).fail(function() {
                    error('Remove fail. Please try again.');
                });
            }
        });

        $(document).on('click', '#cinema-promo .delete', function() {
            var parent = $(this).parent();
            var pid = parent.data('pid');
            var cid = parent.data('cid');

            $.post('/admin/ajax/delete/', {
                type: 'cinema_promo',
                promotion_id: pid,
                cinema_id: cid
            }, function(result) {
                parent.remove();
                success('Remove successfully.');
            }).fail(function() {
                notification('Remove fail. Please try again.');
            });
         });
    })();

    (function() {
        $('table input[data-plugin-ios-switch]').change(function() {
            var tr = $(this).parents('tr');
            $.post('/admin/ajax/active/', {
                table: tr.data('type'),
                field: tr.data('field'),
                id: tr.data('id'),
                is_active: this.checked ? 1 : 0
            });
        });
    })();

    $('#search-select').change(function() {
        $('#search-form').submit();
    });
});

$(document).on('blur', '#artist_name', function() {
       var name = $('#artist_name');
       var parent = name.parents('.form-group');
        $.post('/admin/ajax/checkartist/', {
            s: name.val(),
        }, function(result) {
            if(result){
                alert('Tên arists này đã tồn tại trong hệ thống');
               parent.removeClass('has-success');
               parent.addClass('has-error');
            }else{
               parent.removeClass('has-error');
               parent.addClass('has-success');
            }
        }).fail(function() {

        });
    });

$(document).ready(function(){
    $(".readmore" ).click(function() {
      $(this).parent().find('.description').toggle();
      $(this).parent().find('.shortdescription').toggle();
    });

    $("#checkall").click(function() {
        $(".ios-switch").click();
    });
});