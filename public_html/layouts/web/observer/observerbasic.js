// Store
var db = {
  data_store:{
      0:{"id":0,"name":"FACEBOOK","price":150,"price_after":150},
      1:{"id":1,"name":"GOOGLE","price":200,"price_after":200},
      2:{"id":2,"name":"MICROSOFT","price":300,"price_after":300},
  },
  addDB :function(data_store){
    localStorage.setItem("data", JSON.stringify(data_store));
  },
  getDB:function(){
    return JSON.parse(localStorage.getItem("data"));
  },
  getlength:function(obj) {
    var count = 0;
    for (var k in obj) {
        count++;
    }
    return count;
  },
  getRandom:function(min, max) {
    return Math.random() * (max - min) + min;
  },
  checkDB:function(){
    if(db.getDB() == null){
      db.addDB(db.data_store);
    }else{
      db.data_store =  db.getDB();
    }
    return db.data_store;
  }
};

// build the Observerble base class
var Observerble = ( function( window, undefined ) {
  function Observerble() {this._list = [];}
  Observerble.prototype.attact = function attact( obj ) {this._list.push( obj );};
  Observerble.prototype.detact = function detact( obj ) {
    for( var i = 0, len = this._list.length; i < len; i++ ) {
      if( this._list[ i ] === obj ) {
        this._list.splice( i, 1 );
        return true;
      }
    }
    return false;
  };
  Observerble.prototype.notify = function notify() {
    var args = Array.prototype.slice.call( arguments, 0 );
    for( var i = 0, len = this._list.length; i < len; i++ ) {
      this._list[ i ].update.apply( null, args );
    }
  };
  return Observerble;
} )( window );

// setup an object that fetchs stocks
function tempObservable() {
  var subject = new Observerble();
  this.attact = function attact( obj ) {subject.attact( obj );};
  this.detact = function detact( obj ) {subject.detact( obj );};
  this.pageLoadNotify = function pageLoadNotify() { subject.notify( db.getDB());};
  this.updateNotify = function updateNotify(obj) {subject.notify( obj);};
}

// define a couple of different observers
var UpdateTable = {
  update : function() {
    var el = document.getElementById('table');
    var e = document.createElement('div');
    var msg = "";
    msg = msg + "<table cellspacing='0'><thead><tr><th>Mã chứng khoán</th><th>Giá mua</th><th>Giá hiện tại</th><th>Thay đổi(%)</th></tr></thead><tbody>";
      for( var i in arguments[0] ) {
        var msg_percent = Math.floor((arguments[0][i].price_after - arguments[0][i].price)*100/arguments[0][i].price_after);
        var msg  = msg +"<tr>"
                  +"<td>"+arguments[0][i].name+"</td>"
                  +"<td>"+arguments[0][i].price+"</td>"
                  +"<td class='p_after'>"+arguments[0][i].price_after+"</td>"
                  +"<td class='percent'>"+ msg_percent +"%"+"</td>"
                  +"</tr>";
    }
     msg = msg +"</tbody></table>";
     e.innerHTML = msg;
     $("table").remove();
     el.appendChild(e);

  }
};
var UpdatePriceChange = {
  update : function() {
    var key =  arguments[0].id ;
    var price_after = $(".p_after").eq(key);
    var percent = $(".percent").eq(key);
    var styleHtml = "<span style='background-color:yellow;font-weight:bold;radius:5px'>"
                    +arguments[0].price_after
                    +"</span>";
    var cal_percent = Math.floor((arguments[0].price_after - arguments[0].price)*100/arguments[0].price);

    $(".p_after").find("span").removeAttr('style');
    price_after.html(styleHtml);
    percent.text(cal_percent + "%")
  }
};



$( document ).ready(function() {
  var data_store = db.checkDB();
  // example usage
  var app = new tempObservable();
  app.attact(UpdateTable);
  app.pageLoadNotify();
  app.detact(UpdateTable);
  //End
  app.attact(UpdatePriceChange);

  setInterval(function(){
    var max  = db.getlength(data_store);
    var key = Math.floor((Math.random() * max) + 1);
    key = key - 1;
    data_store[key].price_after = Math.floor(data_store[key].price*db.getRandom(80,150)/100);
    db.addDB(data_store);
    app.updateNotify(data_store[key]);
  }, 1000);


  $( "#form" ).submit(function(e) {
       var newName = $("#name").val();
       var newPrice = $("#price").val();
       var length = db.getlength(data_store);
       $("#notify").text("");
       if(length >=10){
          $("#notify").text("*Vượt quá số mã quy định (>10)");
          return false;
       }

       var newel = {
          'id':length,
          'name':newName,
          'price':newPrice,
          'price_after':newPrice
       };


       data_store[newel.id] = newel;
       db.addDB(data_store);

       app.attact( UpdateTable );
       app.pageLoadNotify(); // console logs: "update" called on StockUpdater with...
       app.detact( UpdateTable );
       e.preventDefault();
  });
});
