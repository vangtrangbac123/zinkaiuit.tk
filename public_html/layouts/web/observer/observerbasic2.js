var localStore = {
    0:{
      "id":0,
      "name":"FACEBOOK",
      "price":150,
      "price_after":150
    },
    1:{
      "id":1,
      "name":"GOOGLE",
      "price":200,
      "price_after":200
    },
     2:{
      "id":2,
      "name":"MICROSOFT",
      "price":300,
      "price_after":300
    },
};
// Store
var db = {
  addDatabase :function(localStore){
    localStorage.setItem("data", JSON.stringify(localStore));
  },

  getDatabase:function(){
    return JSON.parse(localStorage.getItem("data"));
  }
};


if(db.getDatabase() == null){
  db.addDatabase(localStore);
}else{
  localStore =  db.getDatabase();
}

console.log(localStore);


 function getlength(obj) {
    var count = 0;
    for (var k in obj) {
        count++;
    }
    return count;
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

// build the Subject base class
var Subject = ( function( window, undefined ) {

  function Subject() {
    this._list = [];
  }

  // this method will handle adding observers to the internal list
  Subject.prototype.observe = function observeObject( obj ) {
    this._list.push( obj );
  };
  
  Subject.prototype.unobserve = function unobserveObject( obj ) {
    for( var i = 0, len = this._list.length; i < len; i++ ) {
      if( this._list[ i ] === obj ) {
        this._list.splice( i, 1 );
        return true;
      }
    }
    return false;
  };
  
  Subject.prototype.notify = function notifyObservers() {
    var args = Array.prototype.slice.call( arguments, 0 );
    for( var i = 0, len = this._list.length; i < len; i++ ) {
      this._list[ i ].update.apply( null, args );
    }
  };

  return Subject;

} )( window );

// setup an object that fetchs stocks
function User() {
  
  var subject = new Subject();
  
  this.addObserver = function addObserver( newObserver ) {
    subject.observe( newObserver );
  };
  
  this.removeObserver = function removeObserver( deleteObserver ) {
    subject.unobserve( deleteObserver );
  };
  
  this.userNotify = function userNotify() {
    // notify our observers of the stock change
    subject.notify( db.getDatabase());
  };

  this.changeNotify = function changeNotify(obj) {
    // notify our observers of the stock change
    subject.notify( obj);
  };
  
}

// define a couple of different observers
var UpdateTable = {
  update : function() {
    var el = document.getElementById('table');
    var e = document.createElement('div');
    var msg = "";
    msg = msg + "<table cellspacing='0'>"
          +"<thead>"
          +"<tr>"
          +"<th>MÃ£ chá»©ng khoÃ¡n</th>"
          +"<th>GiÃ¡ mua</th>"
          +"<th>GiÃ¡ hiá»‡n táº¡i</th>"
          +"<th>Thay Ä‘á»•i(%)</th>"
          +"</tr>"
          +"</thead>"
          +"<tbody>";

      for( var i in arguments[0] ) {
        var msg_percent = Math.floor((arguments[0][i].price_after - arguments[0][i].price)*100/arguments[0][i].price_after);
        var msg  = msg +"<tr>"
                  +"<td>"+arguments[0][i].name+"</td>"
                  +"<td>"+arguments[0][i].price+"</td>"
                  +"<td class='p_after'>"+arguments[0][i].price_after+"</td>"
                  +"<td class='percent'>"+ msg_percent +"%"+"</td>"
                  +"</tr>";
    }
     msg = msg +"</tbody></table>";
     e.innerHTML = msg;
     $("table").remove();

     el.appendChild(e);




  }
};
var UpdateChart = {
  update : function() {
    var key =  arguments[0].id ;
    var price_after = $(".p_after").eq(key);
    var percent = $(".percent").eq(key);

    $(".p_after").find("span").removeAttr('style');

    price_after.html("<span style='background-color:yellow;font-weight:bold;radius:5px'>"+arguments[0].price_after+"</span>");
    percent.text(Math.floor((arguments[0].price_after - arguments[0].price)*100/arguments[0].price_after)  + "%")
    console.log(key);
  }
};

// example usage
var app = new User();
app.addObserver( UpdateTable );
app.userNotify(); // console logs: "update" called on StockUpdater with...
app.removeObserver( UpdateTable );
$( document ).ready(function() {

  //var a = localStore[0].length;

  app.addObserver( UpdateChart );
  
    setInterval(function(){
    var max  = getlength(localStore);
    var key = Math.floor((Math.random() * max) + 1);
    key = key - 1;
    //key = key - 1;
    localStore[key].price_after = Math.floor(localStore[key].price*getRandomArbitrary(80,150)/100);
    db.addDatabase(localStore);
    app.changeNotify(localStore[key]); }, 1000);


  $( "#form" ).submit(function( event ) {
      console.log("tessss");
       var newName = $("#name").val();
       var newPrice = $("#price").val();
       var length = getlength(localStore);
       $("#notify").text("");
       if(length >=10){
        $("#notify").text("*VÆ°á»£t quÃ¡ sá»‘ mÃ£ quy Ä‘á»‹nh (>10)");
        return false;
       }

       var newel = {
        'id':length,
        'name':newName,
        'price':newPrice,
        'price_after':newPrice
       };


       localStore[newel.id] = newel;
       db.addDatabase(localStore);

       app.addObserver( UpdateTable );
       app.userNotify(); // console logs: "update" called on StockUpdater with...
       app.removeObserver( UpdateTable );
      event.preventDefault();
  });
});
//app.addObserver( UpdateChart );
//app.userNotify(); // console logs: "update" called on StockUpdater with... "update" called on StockCarts with...
//app.removeObserver( UpdateTable );
//app.userNotify(); // console logs: "update" called on StockCharts with...
//app.removeObserver( UpdateChart );
//app.userNotify(); // does nothing; no observers