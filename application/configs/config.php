<?php

class Config_Default {

    const IS_LOCAL       = false;
    const IS_DEVELOPMENT = false;
    const IS_PRODUCTION  = false;


    const PROXY_IP       = '10.30.64.9';
    const PROXY_PORT     = '8888';

    const MEMCACHE_IP    = '10.40.52.11';
    const MEMCACHE_PORT  = '11211';
    const MEMCACHE_TIME  = 300;
    const MEMCACHE_LOG   = true;

    const API_IMG_KEY    = '$@JUYUGOGO';
    const API_IMG_UPLOAD = 'http://upload.123mua.vn/go123/postupload/';
    const API_IMG_RESIZE = 'http://upload.123mua.vn/go123/resize/';

    const STATIC_VERSION = '1.03';
    const STATIC_PATH    = '';

    const LATITUDE_IN_KM            = 0.009009009009009;
    const LONGITUDE_IN_KM           = 0.0091743119266055;

    const DEFAULT_LOCATION_ID       = 1;
    const DEFAULT_LOCATION_DISTANCE = 200; // km
    const DEFAULT_LATITUDE          = 10.773143;
    const DEFAULT_LONGITUDE         = 106.66197;
}
