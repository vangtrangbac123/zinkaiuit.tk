<?php

class Web_Model_Cronjobview extends My_Model_Abstract {

    protected $_name    = 'cronjob_view';
    protected $_primary = 'id';


	 public function insertPlaylistView($arr){
	  	$response = $this->checkPlaylistID($arr["playlist_id"]);

	  	$arr['id'] = "";
	  	if($response){
	  		$arr['id'] = $response->id;
	  	}

  		$this->save($arr);

	 }

	public function checkPlaylistID($playlistId){
		$sql = "
		SELECT id FROM cronjob_view
		WHERE playlist_id =:playlist_id";
		return $this->_db->fetchRow($sql, array("playlist_id" => $playlistId));

	}


}