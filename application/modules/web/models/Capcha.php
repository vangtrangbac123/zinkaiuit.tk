<?php

class Web_Model_Capcha extends My_Model_Abstract {

    protected $_name    = 'capcha';
    protected $_primary = 'capId';
    public function getNewCapcha() {
        $sql = "SELECT *
                FROM capcha
                WHERE status = 0
                ORDER BY capId DESC
                LIMIT 1
                ";

        return $this->_db->fetchRow($sql);
    }


}