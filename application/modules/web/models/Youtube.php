<?php

class Web_Model_Youtube extends My_Model_Abstract {

    protected $_name    = 'youtube';
    protected $_primary = 'youtube_id';

     public function getYoutube($email){
    	$sql = "SELECT
    				y.link,
    				c.is_start
    			FROM youtube  y
    			JOIN comment c ON c.youtube_id = y.youtube_id
    			JOIN users u ON u.user_id = c.user_id
    			WHERE y.is_active = 1 AND u.email= :email
    			ORDER BY y.youtube_id DESC";
    	return $this->_db->fetchRow($sql, array('email' => $email));
    }

    public function getYoutubeOne(){
        $sql = "SELECT link FROM youtube WHERE is_active = 1 ORDER BY date_add DESC";
        return $this->_db->fetchOne($sql);
    }


}

