<?php

class Web_Model_Crawlerlog extends My_Model_Abstract {

    protected $_name    = 'crawler_log';
    protected $_primary = 'stt';

     public function getList($google_id) {
        $sql = 'SELECT * FROM crawler_log   WHERE  auth_google_id = :auth_google_id order by date_add DESC limit 50';
        return $this->_db->fetchAll($sql, array('auth_google_id' => $google_id));
    }


}