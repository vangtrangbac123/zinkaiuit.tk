<?php

class Web_Model_Product extends My_Model_Abstract {

    protected $_name    = 'product';
    protected $_primary = 'product_id';

    public function getListProductByCat($catTd){
    	$sql = "SELECT 	p.product_id,
                        p.product_name,
    				 	p.url_short,
                        p.product_slug,
						i.size2 image
    		   FROM product p
    		   LEFT JOIN product_image pi ON pi.product_id = p.product_id AND pi.type_id = 1
    		   LEFT JOIN images i ON i.image_id = pi.image_id
    		   JOIN categories c ON c.category_id = p.type_id
    		   WHERE  p.is_active = 1 AND p.type_id = :category_id
    		   ORDER BY p.is_hot, p.date_add DESC
    		   LIMIT 10";
    	return $this->_db->fetchAll($sql, array('category_id' => $catTd));
    }

    public function getDetail($productId){
        $sql = "SELECT  p.*,
                        i.size1 image
               FROM product p
               LEFT JOIN product_image pi ON pi.product_id = p.product_id AND pi.type_id = 1
               LEFT JOIN images i ON i.image_id = pi.image_id
               WHERE  p.is_active = 1 AND p.product_id = :product_id";
        return $this->_db->fetchRow($sql, array('product_id' => $productId));
    }

    public function getListRelate($data){
        $sql = "SELECT  p.product_id,
                        p.product_name,
                        p.url_short,
                        p.product_slug,
                        i.size2 image
               FROM product p
               LEFT JOIN product_image pi ON pi.product_id = p.product_id AND pi.type_id = 1
               LEFT JOIN images i ON i.image_id = pi.image_id
               JOIN categories c ON c.category_id = p.type_id
               WHERE  p.is_active = 1 AND p.product_id <> :product_id AND p.type_id = :type_id
               ORDER BY p.is_hot, p.date_add DESC
               LIMIT 5";
        return $this->_db->fetchAll($sql, array('product_id' => $data['product_id'],'type_id' => $data['type_id']));
    }

    public function getListGallery($pId){
        $sql ="SELECT 
                    i.size1,i.size2
               FROM product_image pi
               JOIN images i ON i.image_id = pi.image_id 
               WHERE pi.is_active = 1 AND pi.type_id = 2 AND pi.product_id = :product_id";
        return $this->_db->fetchAll($sql, array('product_id' => $pId));
    }

}