<?php

class Web_Model_Google extends My_Model_Abstract {

    protected $_name    = 'google_api_auth';
    protected $_primary = 'auth_google_id';

    public function getListByUser($userId){
         $sql = "SELECT  *
               FROM google_api_auth
               WHERE  user_id = :user_id
                ORDER BY date_add DESC";
        return $this->_db->fetchAll($sql, array('user_id' => $userId));

    }

    public function getListCare(){
    	$sql = "SELECT  *
               FROM google_api_auth
               WHERE  is_care = 1 AND is_die=0
                ORDER BY date_add DESC";
        return $this->_db->fetchAll($sql);
    }

}