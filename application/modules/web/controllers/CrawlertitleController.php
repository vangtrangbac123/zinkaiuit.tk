<?php

class Web_CrawlertitleController extends My_Controller_Web {
	public $html_content = '';
    public $arr_att_clean = array();

	public function init() {
		$this->_helper->layout->disableLayout();
		//$this->_helper->viewRenderer->setNoRender(true);
		//$this->_request->setActionName('index');
		//header('content-type: application/json; charset=utf-8');
		//header("Access-Control-Allow-Origin: *");
		set_time_limit(10000);
	}

	public function oldAction() {

		$channel = "";
		$string  = "";
		$params = $this->getRequest()->getParams();

		if (isset($params['channel'])) {
	        //$post = $this->_request->getPost();
	        //var_dump($post);die;
	        $channel = str_replace(' ', '+', $params['channel']);

			// lay title bai viet

            $link = "https://www.youtube.com/results?q=".$channel."&sp=";
            //$string = $this->getVideos($link, 'div.yt-lockup', $channel);
            $string = My_Crawler_Htmldom::getVideosTitle($link, 'div.yt-lockup', $channel);


		}
		$this->view->channel = $channel;
        $this->view->string = $string;


	}

	public function indexAction(){
		//My_Crawler_Htmldom::getVideoToAddPlaylist("Mickey Mouse");die;

        //NEXT
        //Videoid => id->videoId
        //Title => snippet->title

        $channel = "";
		$string  = "";
		$params = $this->getRequest()->getParams();

		if (isset($params['channel'])) {
	        //$post = $this->_request->getPost();
	        $channel = $params['channel'];
	        $keyword = $params['channel'];
	       
	         $init = new  Google_Init();
	         $init->updateExpiredToken(1);
	         $service = new Google_Service_YouTube($init->client);

	         //Init Date RFC 3339

	         $d = array(
	            '0' => "week",
	            '1' => "hour",
	            '2' => "day",
	            "3" => "month"
	         );

	         $now = date("Y-m-d H:i:s");
	         $date = strtotime ( '-1 '.$d[3] , strtotime ( $now ) ) ;
	         $publishedAfter = date("c", $date); 



	        $mine = Google_Api::searchListByKeyword($service,
	        'snippet', 
	        array('maxResults' => 50, 'q' => $keyword, 'type' => 'video', 'order' => 'viewCount', 'publishedAfter' => $publishedAfter));

	        $list_video = array();

	        foreach ($mine->items as $key => $video) {
	               $list_video[] = array(
	                    'id' => $video->id->videoId,
	                    'title' => $video->snippet->title,

	               );
	            }   


	        $nextPage = $mine->nextPageToken;

	         //Pagination
	        if(!empty($nextPage)){
	            $next = Google_Api::searchListByKeyword($service,
			        'snippet', 
			        array('maxResults' => 50, 'q' => $keyword, 'type' => 'video', 'order' => 'viewCount', 'publishedAfter' => $publishedAfter, 'pageToken' => $nextPage));


		        foreach ($next->items as $key => $video) {
		               $list_video[] = array(
		                    'id' => $video->id->videoId,
		                    'title' => $video->snippet->title,

		               );
		            } 
	         }
	  

		}
		$this->view->channel = $channel;
        $this->view->string = $list_video;
	}






}
