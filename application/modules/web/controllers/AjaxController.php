<?php

class Web_AjaxController extends My_Controller_Web {

	public function init() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		//$this->_request->setActionName('index');
		header('content-type: application/json; charset=utf-8');
		header("Access-Control-Allow-Origin: *");
	}

	public function getlinkAction(){
		header('content-type: application/json; charset=utf-8');
		header("Access-Control-Allow-Origin: *");

		$params = $this->getRequest()->getParams();

		if(!isset($params['user'])){ 
			echo json_encode(false);
			die;
		}

		$link = $this->model->Youtube->getYoutube($params['user']);
        echo json_encode($link);die;
    }

    public function getlinkoneAction(){
		$link = $this->model->Youtube->getYoutubeOne();
        echo json_encode($link);die;
    }

    public function getreportinfoAction(){
    	$params = $this->getRequest()->getParams();

		if(!isset($params['account'])){ 
			echo json_encode(false);
			die;
		}

		if(!isset($params['source_id'])){
			$params['source_id'] = 0;
		}

		if(!isset($params['is_subcribe'])){
			$params['is_subcribe'] = 0;
		}

    	$data = $this->model->Userreport->getUserReport($params['account'], $params['source_id'], $params['is_subcribe'] );
        echo json_encode($data);die;
    }

    public function autoitAction(){
    	$params = $this->getRequest()->getParams();

		if(!isset($params['account'])){
			echo json_encode(false);
			die;
		}
		if(!isset($params['status'])){
			echo json_encode(false);
			die;
		}

		if(isset($params['autoit'])){
			 $params['status'] = "imacro";
		}

		$data = array('auto_id' => $params['account'],
				 'account' => $params['account'],
				 'status'  => $params['status'],
			);

		$result = $this->model->Autoit->save($data);
        echo json_encode($result);die;

    }
    public function statusautoitAction(){
    	$params = $this->getRequest()->getParams();

		if(!isset($params['account'])){
			echo json_encode(false);
			die;
		}


		$result = $this->model->Autoit->get($params['account']);
		if(!isset($params['autoit'])){
			 echo json_encode($result->status);die;
		}
       echo $result->status;die;

    }

    public function updatereportinfoAction(){
    	$params = $this->getRequest()->getParams();
    	$this->model->Userreport->updateReport($params);
    	echo json_encode(true);die;
    }

    public function delemailAction(){
    	$params = $this->getRequest()->getParams();
    	$r = $this->model->Userreport->updateEmailTerminate($params);
    	echo json_encode($r);die;
    }

    public function rawAction(){
    	$post = @file_get_contents("php://input");
   		$post = (array)@json_decode($post);
    	$parts = explode("=",$post['link']); 
			//break the string up around the "/" character in $mystring 
		$post['youtube_id'] = $parts[1];
		$post['date_add'] = date('Y-m-d H:i:s');
		$this->model->Rawdata->insertIgnore($post);
    	echo json_encode($post);die;

    }

	public function indexAction() {

		$params = $this->getRequest()->getParams();

		if (!isset($params['method'])) die('Request Invalid');

		list($object, $function) = explode('.', $params['method'], 2);

		$path = dirname(__FILE__);

		$file    = "$path/ajax/$object.php";

		if (!file_exists($file)) die('Object not found');

		require_once $file;

		$controller = "Android_Ajax_$object";

		if (!class_exists($controller)) die('Class not found');

		$service = new $controller($object, $this);

		if (!method_exists($service, $function)) die(0); // Function not found

		unset($params['method']);
		unset($params['module']);
		unset($params['action']);
		unset($params['controller']);

		$results = call_user_func(array($service, $function), $params);

		header('content-type: application/json; charset=utf-8');
		echo Zend_Json::encode($results);
		die;
	}
	//VIDEO CRAWLER
	 public function getcrawlervideoAction(){
    	$params = $this->getRequest()->getParams();
		if(!isset($params['user_id']) || !isset($params['email'])){ 
			echo json_encode(false);
			die;
		}

    	$data = $this->model->Crawler->getVideo($params['user_id'], $params['email']);
        echo json_encode($data);die;
    }
    public function updatecrawlerstatusAction(){
    	$params = $this->getRequest()->getParams();
    	$this->model->Crawler->updateStatus($params);
    	echo json_encode(true);die;
    }
    public function updatelogfileAction(){
    	$params = $this->getRequest()->getParams();
    	if(!isset($params['email'])){
			echo json_encode(false);
			die;
		}
    	$this->model->Crawler->updateLog($params);
    	echo json_encode(true);die;
    }
    //END VIDEO CRAWLER
   //SHOW PRIMARY
     public function getlistshowAction(){
    	$params = $this->getRequest()->getParams();
		if(!isset($params['email'])){
			echo json_encode(false);
			die;
		}

    	$data = $this->model->Plsshow->getListVideo($params['email']);
    	if(!empty($data->list_add)){
    		$temp = array_filter(array_map("trim",explode(",", $data->list_add)));
    		$data->list_add = $temp;
    	}

    	if(!empty($data->list_remove)){
    		$temp = array_filter(array_map("trim",explode(",", $data->list_remove)));
    		$data->list_remove = $temp;
    	}
        echo json_encode($data);die;
    }
    //REPORT
    public function gettextreportAction(){
    	 $txt = $this->model->Userreport->getReportTxt();
    	echo json_encode($txt);die;
    }
    //TAKE CARE PLAYLIST
     public function getcarevideoAction(){
    	$params = $this->getRequest()->getParams();
		if(!isset($params['user_id']) || !isset($params['email'])){ 
			echo json_encode(false);
			die;
		}

    	$data = $this->model->Plscare->getVideo($params['email']);
        echo json_encode($data);die;
    }
    public function updatecarestatusAction(){
    	$params = $this->getRequest()->getParams();
    	$this->model->Plscare->updateStatus($params);
    	echo json_encode(true);die;
    }

    public function delplaylistAction(){
    	//Init
    	$params = $this->getRequest()->getParams();
    	if(isset($params['pll_id']) && isset($params['google_id'])){

	    	$auth_google_id = $params['google_id'];
	    	$init = new  Google_Init();
	        $init->updateExpiredToken($auth_google_id);
	        $service = new Google_Service_YouTube($init->client);

	    	$data = Google_Api::playlistsDelete($service,
			    $params['pll_id'], 
			    array('onBehalfOfContentOwner' => ''));

			 echo json_encode($data);die;

		}

		echo json_encode(false);die;
	}

	 public function addplaylistAction(){
    	//Init
    	$params = $this->getRequest()->getParams();
    	if(isset($params['google_id']) && isset($params['pll_title'])){
    		$videos    = preg_split('/\r\n|[\r\n]/', $params['vids_id']);
    		$pll_title = preg_split('/\r\n|[\r\n]/', $params['pll_title']);

    		//Check Aray
    		if(!is_array($pll_title) || !is_array($videos)){
    			echo json_encode(false);die;
    		}

			//Init Client
	         $init = new  Google_Init();
	         //$init->getClientBySession(9);
	         $init->updateExpiredToken($params['google_id']);
	         $service = new Google_Service_YouTube($init->client);

	         //Begin Create
	         foreach ($pll_title as $key => $title) {
	         	if(!empty($title)){
	         		$playlistId = Google_Api::createNewPlaylist($service, $title);
	         		foreach ($videos as $key => $video) {
	         			if(!empty($video)){
	         				$response = Google_Api::addVideo($service, $playlistId, $video);
	         			}
	         		}
	         	}
	         	
	         }
	         echo json_encode(true);die;

		}

		echo json_encode(false);die;
	}
	public function updatecareplaylistAction(){
		$params = $this->getRequest()->getParams();
		if(!isset($params['google_id'])){echo json_encode(false);die;}
		//SET ARRAY
		$arr = array(
			'auth_google_id' => $params['google_id'],
			'is_care' => $params['care'],
			'keyword' => $params['keyword'],
			'add_vids_position' => $params['position'],
			'bydate' => $params['date']
		);

		$s = $this->model->Google->save($arr);
		echo json_encode($s);die;

	}

	public function crawlervideoidAction(){

		$string  = "";
		$params = $this->getRequest()->getParams();

		if (isset($params['keyword']) && !empty($params['keyword'])) {

	        $keyword = $params['keyword'];
	       
	         $init = new  Google_Init();
	         $init->updateExpiredToken(1);
	         $service = new Google_Service_YouTube($init->client);

	         //Init Date RFC 3339

	         $d = array(
	            '0' => "week",
	            '1' => "hour",
	            '2' => "day",
	            "3" => "month"
	         );

	         $now = date("Y-m-d H:i:s");
	         $date = strtotime ( '-3 '.$d[3] , strtotime ( $now ) ) ;
	         $publishedAfter = date("c", $date); 



	        $mine = Google_Api::searchListByKeyword($service,
	        'snippet', 
	        array('maxResults' => 10, 'q' => $keyword, 'type' => 'video', 'order' => 'viewCount', 'publishedAfter' => $publishedAfter));

	        $list_video = array();

	        foreach ($mine->items as $key => $video) {
	               $list_video[] = $video->id->videoId;
	            }  
	        echo json_encode($list_video);die;
	  

		}
		echo json_encode("");die;
	}

	public function crawlervideotitleAction(){

		$string  = "";
		$params = $this->getRequest()->getParams();

		if (isset($params['keyword']) && !empty($params['keyword'])) {

	        $keyword = $params['keyword'];
	       
	         $init = new  Google_Init();
	         $init->updateExpiredToken(1);
	         $service = new Google_Service_YouTube($init->client);

	         //Init Date RFC 3339

	         $d = array(
	            '0' => "week",
	            '1' => "hour",
	            '2' => "day",
	            "3" => "month"
	         );

	         $now = date("Y-m-d H:i:s");
	         $date = strtotime ( '-1 '.$d[3] , strtotime ( $now ) ) ;
	         $publishedAfter = date("c", $date); 



	        $mine = Google_Api::searchListByKeyword($service,
	        'snippet', 
	        array('maxResults' => 50, 'q' => $keyword, 'type' => 'video', 'order' => 'viewCount', 'publishedAfter' => $publishedAfter));

	        $list_video = array();

	        foreach ($mine->items as $key => $video) {
	               $list_video[] = $video->snippet->title;
	            }  

	        $nextPage = $mine->nextPageToken;

	         //Pagination
	        if(!empty($nextPage)){
	            $next = Google_Api::searchListByKeyword($service,
			        'snippet', 
			        array('maxResults' => 50, 'q' => $keyword, 'type' => 'video', 'order' => 'viewCount', 'publishedAfter' => $publishedAfter, 'pageToken' => $nextPage));


		        foreach ($next->items as $key => $video) {
		               $list_video[] = $video->snippet->title;
		            } 
	         }
	        echo json_encode($list_video);die;
	  

		}
		echo json_encode("");die;
	}

	public function getlogAction(){
		$params = $this->getRequest()->getParams();
		if(!isset($params['google_id'])){echo json_encode(false);die;}
		//SET ARRAY
		
		$string = "";
		$s = $this->model->Crawlerlog->getList($params['google_id']);
		if(!empty($s)){
			foreach ($s as $key => $item) {
				$string .= "<br>";
				$string .= $key.".) <b>".$item->channel."</b>";
				$string .= " - Add Video:".$item->video_id;
				$string .= "- Status:".$item->status;
				$string .= "- Message: ".$item->message;
				$string .= "- Date:".$item->date_add;
			}
		}
		echo json_encode($string);die;

	}

}
