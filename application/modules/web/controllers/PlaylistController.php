<?php

class Web_PlaylistController extends My_Controller_Action {

    public function init() {

        if (!isset($_SESSION['login'])) {
            $this->_redirect('/auth');
        }
    }

    public function testAction(){
        $this->updateExpiredToken(9);
        $service = new Google_Service_YouTube($this->client);
        //By Channel ID
        $playlist = $this->playlistsListByChannelId($service,
            'snippet,contentDetails', 
            array('channelId' => 'UC_x5XG1OV2P6uZZ5FSM9Ttw', 'maxResults' => 25));
        //By Myself
        $mine = $this->playlistsListByChannelId($service,
        'snippet,contentDetails', 
        array('mine' => true, 'maxResults' => 25, 'onBehalfOfContentOwner' => '', 'onBehalfOfContentOwnerChannel' => ''));

        var_dump($mine);die;
    }

    public function sessionAction(){
        //Init Client
         $init = new  Google_Init();
         //$init->getClientBySession(9);
         $init->updateExpiredToken(9);
         $service = new Google_Service_YouTube($init->client);
         //End init
         $playlist = array('title' => 'This is test PlayList'.date('y-m-d'), 'description' =>'');
         //$playlistId = Google_Api::createNewPlaylist($service, $playlist );
         $playlistId = "PLmZIurJ7Dodt6HLhGuByJXJMn8UdI_3am";
         $response = Google_Api::addVideo($service, $playlistId, "PhqPvM9Nhys", 3);
         var_dump($response);die;
    }

    public function getitemsAction(){
        $params = $this->getRequest()->getParams();
        if (isset($params['google_id']) && isset($params['pll_id'])) {
            //Init Client
         $init = new  Google_Init();
         $init->updateExpiredToken($params['google_id']);
         $service = new Google_Service_YouTube($init->client);


        $mine = $this->playlistItemsListByPlaylistId($service,
        'snippet,contentDetails', 
        array('maxResults' => 50, 'playlistId' => $params['pll_id']));
        $list_item =array();
        //var_dump($mine->items[0]);die;
        $parent_info = array('channel_name' => $mine->items[0]->snippet->channelTitle, 'google_id'=> $params['google_id']);
        foreach ($mine->items as $key => $item) {
               $list_item[] = array(
                    'id' => $item->snippet->resourceId->videoId,
                    'title' => $item->snippet->title,
                    'description' => $item->snippet->description
               );
            }   


        $this->view->list_item = $list_item;
        $this->view->parent_info = $parent_info;
        }
    }

    public function indexAction() {
        $params = $this->getRequest()->getParams();

        if (isset($params['code'])) {
            $init = new  Google_Init();
            $init->insertToken($params['code']);
        }

        $init = new  Google_Init();
        $url = $init->client->createAuthUrl();
 

        $list = $this->model->Google->getListByUser($_SESSION['login']->user_id);
        $this->view->listChannel = $list;
        $this->view->authGO = $url;
        // var_dump("Nothing");die;
    }

    public function getlistAction(){
        $params = $this->getRequest()->getParams();
        if (isset($params['google_id'])) {
            //Init Client
         $init = new  Google_Init();
         $init->updateExpiredToken($params['google_id']);
         $service = new Google_Service_YouTube($init->client);
            //By Myself
        $mine = $this->playlistsListByChannelId($service,
        'snippet,contentDetails', 
        array('mine' => true, 'maxResults' => 25, 'onBehalfOfContentOwner' => '', 'onBehalfOfContentOwnerChannel' => ''));
        }
        $list_playlist = array();
        //var_dump($mine->items[0]);die;
        $parent_info = array('channel_name' => $mine->items[0]->snippet->channelTitle, 'google_id'=> $params['google_id'], 'total' => $mine->pageInfo->totalResults);



        foreach ($mine->items as $key => $playlist) {
               $list_playlist[] = array(
                    'id' => $playlist->id,
                    'title' => $playlist->snippet->title,
                    'description' => $playlist->snippet->description,
                    'item_count'  => $playlist->contentDetails->itemCount

               );
            }   

                //Total Playlist
        $total = $mine->pageInfo->totalResults;
        $page = CEIL($total/25);
        $nextPage = $mine->nextPageToken;

         //Pagination
        for($i = 1; $i < $page;$i++){

            $next = Google_Api::playlistsListByChannelId($service,
                    'snippet,contentDetails', 
                    array('mine' => true, 'maxResults' => 25,'onBehalfOfContentOwner' => '', 'onBehalfOfContentOwnerChannel' => '', 'pageToken' => $nextPage));

             //var_dump($mine->items[0]);die;
            foreach ($next->items as $key => $playlist) {
                   $list_playlist[] = array(
                        'id' => $playlist->id,
                        'title' => $playlist->snippet->title,
                        'description' => $playlist->snippet->description,
                        'item_count'  => $playlist->contentDetails->itemCount

                   );
                }
             $nextPage = $next->nextPageToken;

        }



        $this->view->list_playlist = $list_playlist;
        $this->view->parent_info = $parent_info;
    }


    // Sample php code for playlists.list

    public function playlistsListByChannelId($service, $part, $params) {
        $params = array_filter($params);
        $response = $service->playlists->listPlaylists(
            $part,
            $params
        );

        return $response;
    }
    //// Sample php code for playlistItems.list
    public function playlistItemsListByPlaylistId($service, $part, $params) {
        $params = array_filter($params);
        $response = $service->playlistItems->listPlaylistItems(
            $part,
            $params
        );

        return $response;
    }



}