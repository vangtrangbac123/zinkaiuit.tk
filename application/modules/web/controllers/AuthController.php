<?php

class Web_AuthController extends My_Controller_Web{

    public $auth;

    public function init() {
        $this->_helper->layout()->setLayout('login');
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction() {

        if (isset($_SESSION['login'])) {
            $this->_redirect('/playlist');
        }

        if ($this->_request->isPost()) {

            $post = $this->_request->getPost();
            $post = array_map('trim', $post);

            if (!empty($post['username']) && !empty($post['password'])) {

                // check user exists
                $user = $this->model->Users->get(array('username' => $post['username']));

                if ($user) {

                    if (!empty($user->password) && $user->password == md5($post['password'])) {

                       $result = true;

                    } else {

                       $result = false;
                    }

                    if ($result) {

                        $user->fullname = trim($user->fullname);
                        $user->user_id  = intval($user->user_id);
                        $user->level    = $user->level;

                        if (empty($user->fullname)) {
                            $user->fullname = $user->username;
                        }

                        $_SESSION['login'] = $user;
                        $this->_redirect('/playlist');
                    }
                }
            }

            $this->view->error    = 'Wrong username or password.';
            $this->view->username = $post['username'];
            $this->view->password = $post['password'];
        }

        // if (!App::isProduction()) {

        //     // transparent login
        //     $user = new stdClass;
        //     $user->username = 'admin';
        //     $user->fullname = 'Admin';
        //     $user->level    = 10;
        //     $user->user_id  = -1;
        //     $user->is_admin = 1;

        //     $this->auth->getStorage()->write($user);
        //     $this->_redirect('/');
        // }
    }



    public function logoutAction() {

        if ($_SESSION['login']) {
            unset($_SESSION['login']);
        }

        $this->_redirect('/auth');
    }

}
