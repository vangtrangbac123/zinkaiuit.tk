<div class="logo">
    <img src="http://lorempixel.com/output/people-q-g-64-64-1.jpg" class="img-responsive center-block" alt="Logo">
       
</div>
<br>
<div class="left-navigation">
    <ul class="list">
        <h5><strong>ACCOUNT</strong></h5>
        <li>
         <?php if(isset($_SESSION["login"])):?>
        Hello, <?php echo $_SESSION["login"]->username?> - <a href="/auth/logout">Logout</a>
      <?php else:?>
        <a href="/auth"><span class="glyphicon glyphicon-log-in"></span> Login</a>
      <?php endif;?></li>
    </ul>

    <br>

    <ul class="list">
        <h5><strong>MENU</strong></h5>
        <li>Hiking</li>
        <li>Rafting</li>
        <li>Badminton</li>
        <li>Tennis</li>
        <li>Sketching</li>
        <li>Horse Riding</li>
    </ul>
</div>