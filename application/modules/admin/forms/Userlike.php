<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);


$fields['email'] = array(
    'label' => 'Email',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['pass'] = array(
    'label' => 'Pass',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$listview = array(
    'part' => 'userlike',
    'colums' => array('#', 'Email', 'Pass', 'Active', 'Action')
);

$list = array();
$list['model'] = 'Userreport';
$list['form']  = 'Userlike';
$list['table'] = 'user_report';
$list['primary'] = 'user_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;