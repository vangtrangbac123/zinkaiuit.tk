<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['position_id'] = array(
    'label' => 'Position',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListPositionBanner(),
);

$fields['banner_url'] = array(
    'label' => 'Link',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['banner_title'] = array(
    'label' => 'Title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['@banner_image'] = array(
    'label' => 'Image',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_IMAGE,
    'image' => array(
        'width' => 200,
        'height' => 100,
        'type' => 1,
        'size' => 1,
    )
);


$listview = array(
    'part' => 'banner',
    'colums' => array('#', 'Image', 'Title', 'Position', 'Active', 'Action')
);

$list = array();
$list['model'] = 'Banner';
$list['form']  = 'Banner';
$list['table'] = 'banner_v4';
$list['primary'] = 'banner_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;