<?php

$fields = array();


$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['show_name'] = array(
    'label' => 'Show Name',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);
$fields['list_add'] = array(
    'label' => 'List Add',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['list_remove'] = array(
    'label' => 'List Remove',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['list_email'] = array(
    'label' => 'List Email',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);
$fields['is_delete'] = array(
    'label' => 'CO Delete Video Bi Xoa Hay ko',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);




$listview = array(
    'part' => 'plsshow',
    'colums' => array('#', 'show_name', 'list_email', 'Date update', 'Active', 'Action')
);

$list = array();
$list['model'] = 'Plsshow';
$list['form']  = 'Plsshow';
$list['table'] = 'playlist_show';
$list['primary'] = 'show_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;