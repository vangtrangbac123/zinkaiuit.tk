<?php

$fields = array();


$fields['number'] = array(
    'label' => 'Status',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListStatus(),
);

$fields['payment_id'] = array(
    'label' => 'Payment',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListPayment(),
);

$fields['email'] = array(
    'label' => 'Email',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['phone'] = array(
    'label' => 'Phone',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['pass'] = array(
    'label' => 'Pass',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['chanel_name'] = array(
    'label' => 'Chanel Name',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['chanel_link'] = array(
    'label' => 'Chanel Link',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['chanel_id'] = array(
    'label' => 'Chanel_id',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['note'] = array(
    'label' => 'Note',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['is_vps'] = array(
    'label' => 'Is VPS ??',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);


$fields['ip'] = array(
    'label' => 'IP',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['is_adsense'] = array(
    'label' => 'Is Adsense ??',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['is_view'] = array(
    'label' => 'Buy View ??',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);


$fields['date_create'] = array(
    'label' => 'Date Create',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_DATEBOX
);



$listview = array(
    'part' => 'chanel',
    'colums' => array('#', 'Chanel Name', 'Email', 'Link',  'Payment', 'Buy View', 'VPS/IP', 'Action')
);

$list = array();
$list['model'] = 'Chanel';
$list['form']  = 'chanel';
$list['table'] = 'chanel';
$list['primary'] = 'id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;