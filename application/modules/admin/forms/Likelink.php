<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['is_subcribe'] = array(
    'label' => 'Is Subcribe',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);





$fields['link'] = array(
    'label' => 'Link',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['title'] = array(
    'label' => 'Title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['order'] = array(
    'label' => 'Priority',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['like_number'] = array(
    'label' => 'Number like',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['share_number'] = array(
    'label' => 'Number share',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['comment_number'] = array(
    'label' => 'Number comment',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['subcribe_number'] = array(
    'label' => 'Number subcribe',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$listview = array(
    'part' => 'likelink',
    'colums' => array('#', 'Link', 'Title', 'Number User', 'Priority', 'Active', 'Action')
);


$list = array();
$list['model'] = 'Reportlink';
$list['form']  = 'likelink';
$list['table'] = 'report_link';
$list['primary'] = 'youtube_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;