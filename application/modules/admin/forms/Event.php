<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['event_title'] = array(
    'label' => 'title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);
$fields['event_url'] = array(
    'label' => 'url',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);
$fields['event_description'] = array(
    'label' => 'description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['date_show'] = array(
    'label' => 'description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_DATEBOX
);

$listview = array(
    'part' => 'event',
    'colums' => array('#',  'Name', 'date', 'Active')
);

$list = array();
$list['model'] = 'Event';
$list['form']  = 'Event';
$list['table'] = 'event';
$list['primary'] = 'event_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;