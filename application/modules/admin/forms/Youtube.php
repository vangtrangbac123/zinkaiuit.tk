<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['link'] = array(
    'label' => 'Link',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['title'] = array(
    'label' => 'Title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$listview = array(
    'part' => 'youtube',
    'colums' => array('#', 'Link', 'Title', 'Active', 'Action')
);

$list = array();
$list['model'] = 'Youtube';
$list['form']  = 'youtube';
$list['table'] = 'youtube';
$list['primary'] = 'youtube_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;