<?php

$fields = array();


$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['text_value'] = array(
    'label' => 'Text Value',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['email'] = array(
    'label' => 'Email',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['channel'] = array(
    'label' => 'channel',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);
$fields['channel_name'] = array(
    'label' => 'channel Name',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);
$fields['videos_position'] = array(
    'label' => 'Video Position (Vi tri khi Add)',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListPostionVideo(),
);
$fields['computer_id'] = array(
    'label' => 'Computer',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);
$fields['order'] = array(
    'label' => 'Priority',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);




$listview = array(
    'part' => 'crawlertext',
    'colums' => array('#', 'text_value', 'Email', 'Channel', 'Computer', 'Order', 'Active', 'Action')
);

$list = array();
$list['model'] = 'Crawler';
$list['form']  = 'Crawler';
$list['table'] = 'crawler_text_search';
$list['primary'] = 'text_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;