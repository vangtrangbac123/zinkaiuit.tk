<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['payment_name'] = array(
    'label' => 'Payment Name',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['email'] = array(
    'label' => 'Email',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$listview = array(
    'part' => 'payment',
    'colums' => array('#', 'Computer', 'Email', 'Active', 'Action')
);

$list = array();
$list['model'] = 'Payment';
$list['form']  = 'Payment';
$list['table'] = 'payment';
$list['primary'] = 'id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;