<?php

$fields = array();


$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['video_id'] = array(
    'label' => 'Video ID',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);
$fields['video_name'] = array(
    'label' => 'Video Name',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['list_email'] = array(
    'label' => 'List Email',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);
$fields['videos_position'] = array(
    'label' => 'Video Position (Vi tri khi Add)',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListPostionVideo(),
);


$listview = array(
    'part' => 'plscare',
    'colums' => array('#', 'video_name','video ID', 'status', 'Date update', 'Active', 'Action')
);

$list = array();
$list['model'] = 'Plscare';
$list['form']  = 'Plscare';
$list['table'] = 'playlist_care';
$list['primary'] = 'v_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;