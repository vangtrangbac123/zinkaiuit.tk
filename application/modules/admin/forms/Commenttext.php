<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['type_id'] = array(
    'label' => 'Report Type',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListCommentType(),
);

$fields['report_text'] = array(
    'label' => 'Link',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTAREA,
);



$listview = array(
    'part' => 'commenttext',
    'colums' => array('#', 'Text','Active', 'Action')
);

$list = array();
$list['model'] = 'Reporttext';
$list['form']  = 'Commenttext';
$list['table'] = 'report_text';
$list['primary'] = 'report_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;