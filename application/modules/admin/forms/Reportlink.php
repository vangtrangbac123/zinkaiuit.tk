<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['type_id'] = array(
    'label' => 'Report Type',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListReportType(),
);

$fields['link'] = array(
    'label' => 'Link',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['title'] = array(
    'label' => 'Title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['order'] = array(
    'label' => 'Priority',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['number_user'] = array(
    'label' => 'Number User Rp',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$listview = array(
    'part' => 'reportlink',
    'colums' => array('#', 'Link', 'Title', 'Number User', 'Priority', 'Active', 'Action')
);

$list = array();
$list['model'] = 'Reportlink';
$list['form']  = 'reportlink';
$list['table'] = 'report_link';
$list['primary'] = 'youtube_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;