<?php

class Admin_ChanelController extends My_Controller_Form {

    public $_form = 'Chanel';

    public function getList() {
        $where = "";

        $userId  = $this->_getParam('account',  $this->user->user_id);

        if(isset($this->user->user_id) && $userId != 0){
             $where  = "AND c.account = ".$userId;
        }
        //
        $payment_id  = $this->_getParam('payment',  0);

        if($payment_id != 0){
             $where  = "AND c.payment_id = ".$payment_id;
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS c.*,p.payment_name
                FROM chanel c
                JOIN payment p ON p.id = c.payment_id
                WHERE  1 = 1 $where
                ORDER BY  c.number ASC";
        //return $this->model->Chanel->getRows($sql);

        $page = abs((int)$this->_getParam('page', 1));
        $count = abs((int)$this->_getParam('count', 20));
        $offset = ($page - 1) * $count;
        $sql .= " LIMIT $offset, $count";
        $url = sprintf('/%s/%s/page/', $this->controllerName, $this->actionName) . '%d/';

        $data = $this->model->Chanel->getRowsAndTotal($sql, null);

        if (isset($data['total'])) {
            $this->view->pagination = Utility_Paginator::create($url, $page, $count, $data['total']);
        }

        $this->view->chanel = $data['rows'];
        $this->view->user_id = $userId;
        $this->view->payment_id = $payment_id;

    }

     public function onSaveBefore($data, $post) {
        if(isset($this->user->user_id)){
             $data['account'] = $this->user->user_id;
        }
        return $data;
    }

}