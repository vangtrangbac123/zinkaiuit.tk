<?php

class Admin_CommenttextController extends My_Controller_Form {

    public $_form = 'Commenttext';

    public function getDetail($id) {
        return $this->model->Reporttext->get($id);
    }

    public function getListByType($type = 0) {
        $where = "";

        if(isset($this->user->user_id)){
             $where  = "AND account = ".$this->user->user_id;
        }

        $sql = "SELECT * FROM report_text WHERE type_id = $type AND source_id = 1 $where";
        return $this->model->Reporttext->getRows($sql);
    }

    public function staticAction(){
        $this->_helper->viewRenderer('index');
        $this->view->form = Admin_Model_Form::get($this->_form);
         $this->view->list = $this->getListByType(4);
    }

    public function dynamicAction(){
        $this->_helper->viewRenderer('index');
        $this->view->form = Admin_Model_Form::get($this->_form);
        $this->view->list = $this->getListByType(5);

    }


    public function onSaveBefore($data, $post) {
        if(isset($this->user->user_id)){
             $data['account'] = $this->user->user_id;
        }

        $data['source_id'] = 1;
        return $data;
    }

}