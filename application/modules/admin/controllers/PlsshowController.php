<?php

class Admin_PlsshowController extends My_Controller_Form {

    public $_form = 'Plsshow';

    public function getListByAccount($type = 0) {
        $where = "";


        $sql = "SELECT * FROM playlist_show cr WHERE  1 = 1 $where ORDER BY cr.date_add DESC";
        return $this->model->Plsshow->getRows($sql);
    }

    public function indexAction(){
        $this->view->form = Admin_Model_Form::get($this->_form);
         $this->view->list = $this->getListByAccount(0);
    }



    public function onSaveBefore($data, $post) {
         $data['date_update'] = $data['date_add'] = date('Y-m-d H:i:s');
        return $data;
    }


}