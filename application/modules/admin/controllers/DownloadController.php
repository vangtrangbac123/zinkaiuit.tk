<?php

class Admin_DownloadController extends My_Controller_Form {

    public $_form = 'Download';
    public function indexAction(){

        $from  = $this->_getParam('from',  date('Y-m-d',(strtotime ( '-7 day' , strtotime (date('Y-m-d')) ) )));
        $to    = $this->_getParam('to',  date('Y-m-d'));
        $text  = $this->_getParam('text',  '');
        $where = "DATE(date_add) >= '$from' AND DATE(date_add) <= '$to'";
        $where1 ="";

        if(!empty($text)){
             $where .= "AND text_search = '$text'";
        }

        if(isset($this->user->user_id)){
            $where .= " AND account = ".$this->user->user_id;
            $where1 .= "AND account = ".$this->user->user_id;;
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM raw_data WHERE $where  ORDER BY date_add desc";
        $sql1 = "SELECT DISTINCT(text_search) FROM raw_data WHERE 1=1 $where1";

        $page = abs((int)$this->_getParam('page', 1));
        $count = abs((int)$this->_getParam('count', 20));
        $offset = ($page - 1) * $count;
        $sql .= " LIMIT $offset, $count";
        $url = sprintf('/%s/%s/page/', $this->controllerName, $this->actionName) . '%d/';

        $data = $this->model->Download->getRowsAndTotal($sql, null);

        if (isset($data['total'])) {
            $this->view->pagination = Utility_Paginator::create($url, $page, $count, $data['total']);
        }


        $this->view->youtube = $data['rows'];
        $this->view->from = $from;
        $this->view->to = $to;
        $this->view->text = $text;
        $this->view->text_select = $this->model->Download->getRows($sql1);

    }

     public function searchAction(){

        $search = $this->_getParam('s',  '');

        $sql = "SELECT *
		        FROM raw_data
		        WHERE  youtube_id ='$search' OR link = '$search' OR title ='$search'
		        ORDER BY date_add desc";

        $this->view->youtube = $this->model->Download->getRows($sql);

    }




    // public function onSaveBefore($data, $post) {
    // 	$data['type'] = 1;
    // 	return $data;
    // }

}

