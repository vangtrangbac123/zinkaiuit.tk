<?php

class Admin_LikelinkController extends My_Controller_Form {

    public $_form = 'Likelink';

    public function getDetail($id) {
        return $this->model->Reportlink->get($id);
    }


    public function getList() {
        $where = "";

        if(isset($this->user->user_id)){
             $where  = "AND account = ".$this->user->user_id;
        }

        $sql = "SELECT * FROM report_link rl WHERE  source_id = 1 $where ORDER BY rl.order DESC";
        return $this->model->Reportlink->getRows($sql);
    }

    public function onSaveBefore($data, $post) {

        $list_number = array(intval($data['like_number']), intval($data['share_number']) , intval($data['comment_number']), intval($data['subcribe_number']));

        $data['number_user'] = max($list_number);
        if(isset($this->user->user_id)){
             $data['account'] = $this->user->user_id;
        }

        $data['source_id']  = 1;
        $arr_watch = array(0.5,1,1.2,1.3,1.4,2,2.1,2.5);
        $data['watch'] = $arr_watch[array_rand($arr_watch,1)];

        return $data;
    }
    public function onSaveAfter($id, $data) {
        $number_user = $data['number_user'];
        $account = $data['account'];
        $sql = "SELECT user_id 
                FROM user_report
                WHERE is_active = 1
                    AND source_id = 1 
                    AND account = $account 
                ORDER BY RAND() 
                LIMIT ".$number_user;

    	$list_user = $this->model->Userreport->getRows($sql);
        $report_type = 4;
        $this->updateTags($id, $list_user, $report_type);
        return $data;
    }

    private function updateTags($yId, $list_user, $report_type) {
        $yId = intval($yId);
        if ($yId == 0) return;

        $this->model->Userreport->deleteTags(array(
            'table' => 'user_report_link',
            'youtube_id' => $yId,
        ));

        $report_text =  $this->model->Userreport->getAllReport($report_type);


        $sep = '';
        $sql = 'INSERT IGNORE INTO user_report_link (`youtube_id`, `user_id`, `report_text_id`) VALUES ';
        foreach ($list_user as $key => $user) {

            if(isset($report_text{$key})){
                $report_id = $report_text{$key}->report_id;
            }else{
                $report_id = $this->model->Userreport->getOneReport($report_type);
            }

            $sql .= $sep . "($yId, $user->user_id, $report_id)";
            $sep = ', ';
        }

        return $this->model->Userreport->_excute($sql);
    }



}