<?php

class Admin_UserreportController extends My_Controller_Form {

    public $_form = 'Userreport';

    public function getList(){
    	$where = "";
    	if(isset($this->user->user_id)){
             $where  = "AND account = ".$this->user->user_id;
        }
    	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM user_report WHERE 1 = 1 $where and  source_id = 0 ORDER BY user_id DESC";
        return $this->getListAutoPaging($sql);
    }

    public function onSaveBefore($data, $post) {

        if(isset($this->user->user_id)){
             $data['account'] = $this->user->user_id;
        }

        return $data;
    }

}