<?php

class Admin_PlscareController extends My_Controller_Form {

    public $_form = 'Plscare';

    public function getListByAccount($type = 0) {
        $where = "";


        $sql = "SELECT * FROM playlist_care cr WHERE  1 = 1 $where ORDER BY cr.date_add DESC";
        return $this->model->Plsshow->getRows($sql);
    }

    public function indexAction(){
        $this->view->form = Admin_Model_Form::get($this->_form);
         $this->view->list = $this->getListByAccount(0);
    }



    public function onSaveBefore($data, $post) {
         $data['date_update'] = $data['date_add'] = date('Y-m-d H:i:s');
         $data['status_id'] =1;
        return $data;
    }


}