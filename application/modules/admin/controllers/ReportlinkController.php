<?php

class Admin_ReportlinkController extends My_Controller_Form {

    public $_form = 'Reportlink';

    public function getListByType($type = 0) {
        $where = "";

        if(isset($this->user->user_id)){
             $where  = "AND account = ".$this->user->user_id;
        }

        $sql = "SELECT * FROM report_link rl WHERE  type_id = $type AND source_id = 0 $where ORDER BY rl.order DESC";
        return $this->model->Reportlink->getRows($sql);
    }

    public function indexAction(){
        $this->view->form = Admin_Model_Form::get($this->_form);
         $this->view->list = $this->getListByType(0);
    }

    public function advertiseAction(){
        $this->_helper->viewRenderer('index');
        $this->view->form = Admin_Model_Form::get($this->_form);
        $this->view->list = $this->getListByType(1);

    }
    public function thumbnailAction(){
        $this->_helper->viewRenderer('index');
        $this->view->form = Admin_Model_Form::get($this->_form);
        $this->view->list = $this->getListByType(2);

    }
    public function scamsAction(){
        $this->_helper->viewRenderer('index');
        $this->view->form = Admin_Model_Form::get($this->_form);
        $this->view->list = $this->getListByType(3);

    }


    public function onSaveBefore($data, $post) {

        if(empty($data['number_user']) || !is_numeric($data['number_user'])){
             $data['number_user'] = 25;
        }

        if(isset($this->user->user_id)){
             $data['account'] = $this->user->user_id;
        }
        return $data;
    }
    public function onSaveAfter($id, $data) {
        $number_user = $data['number_user'];
        $account = $data['account'];
        $sql = "SELECT user_id 
                FROM user_report
                WHERE is_active = 1
                    AND source_id = 0
                    AND source_id = 0 
                    AND account = $account 
                ORDER BY RAND() 
                LIMIT ".$number_user;
        $list_user = $this->model->Userreport->getRows($sql);
        $report_type = $data['type_id'];
        $this->updateTags($id, $list_user, $report_type);
        return $data;
    }

    private function updateTags($yId, $list_user, $report_type) {
        $yId = intval($yId);
        if ($yId == 0) return;

        $this->model->Userreport->deleteTags(array(
            'table' => 'user_report_link',
            'youtube_id' => $yId,
        ));

        //Ep kieu ve type =0  text 
        $report_type = 0;


        $report_text =  $this->model->Userreport->getAllReport($report_type);


        $sep = '';
        $sql = 'INSERT IGNORE INTO user_report_link (`youtube_id`, `user_id`, `report_text_id`) VALUES ';
        foreach ($list_user as $key => $user) {

            if(isset($report_text{$key})){
                $report_id = $report_text{$key}->report_id;
            }else{
                $report_id = $this->model->Userreport->getOneReport($report_type);
            }

            $sql .= $sep . "($yId, $user->user_id, $report_id)";
            $sep = ', ';
        }

        return $this->model->Userreport->_excute($sql);
    }



}