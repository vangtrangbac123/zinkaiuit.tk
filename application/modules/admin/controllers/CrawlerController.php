<?php

class Admin_CrawlerController extends My_Controller_Form {

    public $_form = 'Crawler';

    public function getListByAccount($type = 0) {
        $where = "";

        if(isset($this->user->user_id)){
             $where  = "AND user_id = ".$this->user->user_id;
        }

        $sql = "SELECT * FROM crawler_text_search cr WHERE  1 = 1 $where ORDER BY cr.computer_id,cr.order,cr.date_add DESC";
        return $this->model->Crawler->getRows($sql);
    }

    public function indexAction(){
        $this->view->form = Admin_Model_Form::get($this->_form);
         $this->view->list = $this->getListByAccount(0);
    }



    public function onSaveBefore($data, $post) {

        // if(empty($data['number_user']) || !is_numeric($data['number_user'])){
        //      $data['number_user'] = 25;
        // }

        $data['email'] = Utility_Validate::trimEmail($data['email']);

        if(isset($this->user->user_id)){
              $data['user_id'] = $this->user->user_id;
         }
         $data['date_update'] = $data['date_add'] = date('Y-m-d H:i:s');
        return $data;
    }


}