<?php

class Admin_UserlikeController extends My_Controller_Form {

    public $_form = 'Userlike';

     public function getDetail($id) {
        return $this->model->Userreport->get($id);
    }

    public function getList(){
    	$where = "";
    	if(isset($this->user->user_id)){
             $where  = "AND account = ".$this->user->user_id;
        }
    	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM user_report WHERE 1 = 1 $where and  source_id = 1 ORDER BY user_id ASC";
    	//return $this->model->Userreport->getRows($sql);
         return $this->getListAutoPaging($sql);
    }

    public function onSaveBefore($data, $post) {

        if(isset($this->user->user_id)){
             $data['account'] = $this->user->user_id;
        }
        $data['source_id'] = 1;

        return $data;
    }

}