<?php

class Admin_Go123Controller extends Zend_Controller_Action {

    public   $max_filesize_upload;
    public   $min_width;
    public   $min_height;
    public   $upload_path;
    public   $original_folder;
    public   $thumb_folder;
    public   $media_folder;
    public   $extenion;
    public   $file_error_log_path;
    public   $log_path;
    public   $userName;
    public   $publicDomain;
    public   $allowExtensions;
    
    public     $security_key = 'aBcXyZ123';
    public     $security_key_app = '@#upload_app_aBx#@';
    public   $error_message = array(
                                    0=>'Upload Successful',
                                    1=>'Tên file không hợp lệ',
                                    2=>'Kích thước ảnh quá nhỏ hoặc quá lớn',
                                    3=>'Ảnh quá dài hoặc quá ốm',
                                    4=>'Kiểu file không hợp lệ',
                                    5=>'Upload file thất bại',
                                    6=>'Upload file thất bại'
    );

    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->max_filesize_upload = 10*1024*1024;
        $this->min_width = 50;
        $this->min_height = 50;

        $this->publicDomain =  isset($_SERVER['HTTP_HOST'])?'http://'.$_SERVER['HTTP_HOST']:DOMAIN;

        $this->upload_path = UPLOAD_PATH;
        $this->media_folder = 'media';
        $this->log_path = '';
        $this->allowExtensions = array('jpg','jpeg','png','gif');
        $this->extenion = '.jpg';
        $this->security_key = 'b649ba5fa9aa';
    }

    /**
     *
     * Default action
     */
    public function indexAction() {
        $signKey = $this->_getParam('signkey', '');
        $time = $this->_getParam('time', '');
        //$upload = new Utility_Upload();
        /*if(!$this->verifySignKey($signKey, $time))
        {
            //echo "Invalid data [". LOGIN_NAME ."]";
            //exit;
        }*/
        $this->view->signKey = $signKey;
        $this->view->time = $time;
    }
    public function uploadAction()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_FILES)){
            //$upload = new Utility_Upload();
    
            if(APPLICATION_ENV == 'production')
            {
                $atomicResize = false;
            }
            else
            {
                $atomicResize = true;
            }
    
            $rs = $this->upload123go($atomicResize);
    
            $rt = array();
    
            $errorCode = array(
                    '1' => 'File không hợp lệ',
                    '2' => 'Kích thước ảnh vượt qua kích thước cho phép',
                    '3' => 'Kích thước ảnh quả nhỏ',
                    '4' => 'File không hợp lệ',
                    '5' => 'Xảy ra lỗi ra trong quá trình upload (5)',
                    '6' => 'Vui lòng đăng nhập để upload ảnh',
                    '7' => 'Xảy ra lỗi ra trong quá trình upload (7)',
                    '8' => 'Xảy ra lỗi ra trong quá trình upload (8)',
            );
            if(isset($rs[0]['error_code']))
            {
                $rt = array(array(
                        'name'  => isset($rs[0]['name']) ? $rs[0]['name'] : '',
                        'size'  => isset($rs[0]['size']) ? $rs[0]['size'] : '',
                        'error' => isset($errorCode[$rs[0]['error_code']]) ? $errorCode[$rs[0]['error_code']] : ''
                ));
            }
            else
            {
                $rt = array(array(
                        'name'  => $rs[0]['name'],
                        'size'  => $rs[0]['size'],
                        'url'   => $rs[0]['url']
                ));
            }
    
            echo Zend_Json::encode($rt); exit;
        }
        else
        {
            echo Zend_Json::encode(array()); exit;
        }
    }

    /**
     * Verify security key
     * @param unknown_type $key
     * @param unknown_type $width
     * @param unknown_type $height
     * @return boolean
     */
    public  function verifySignKey($key, $time, $width = 0, $height = 0, $maxSize = 0)
    {
        $myKey = md5($this->security_key .$time. $width . $height . $maxSize);

        if($myKey == $key)
            return true;
        
        return true;
    }

    
    public function postuploadAction()
    {
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
            
            if(!isset($_POST['file_data']) || !$_POST['file_name'] || $_POST['file_name'] == '' || !$_POST['seckey'])
            {
                echo Zend_Json::encode(array()); exit;
            }
            
            $privateKey = '$@JUYUGOGO';
            $fileName = trim($_POST['file_name']);
            $seckey = trim($_POST['seckey']);
            
            $myseckey = md5($_POST['file_name'] . $privateKey);

            if($seckey != $myseckey)
            {
                echo Zend_Json::encode(array()); exit;
            }

            $upload = new Upload();
    
            if(APPLICATION_ENVIRONMENT == 'production')
            {
                $atomicResize = false;
            }
            else
            {
                $atomicResize = true;
            }
            
            $data = array(
                        'file_name' => isset($_POST['file_name']) ? trim($_POST['file_name']) : ''                      
            );
            
            $rs = $upload->saveRawData($_POST['file_data'], $data, array('123phim', date('Y'), date('m')));
    
            $rt = array();
    
            $errorCode = array(
                    '1' => 'File không hợp lệ',
                    '2' => 'Kích thước ảnh vượt qua kích thước cho phép',
                    '3' => 'Kích thước ảnh quả nhỏ',
                    '4' => 'File không hợp lệ',
                    '5' => 'Xảy ra lỗi ra trong quá trình upload (5)',
                    '6' => 'Vui lòng đăng nhập để upload ảnh',
                    '7' => 'Xảy ra lỗi ra trong quá trình upload (7)',
                    '8' => 'Xảy ra lỗi ra trong quá trình upload (8)',
            );
            if(isset($rs['error_code']))
            {
                $rt = array(array(
                        'name'  => isset($rs['name']) ? $rs['name'] : '',
                        'size'  => isset($rs['size']) ? $rs['size'] : '',
                        'error' => isset($errorCode[$rs['error_code']]) ? $errorCode[$rs['error_code']] : ''
                ));
            }
            else
            {
                $rt = array(array(
                        'name'  => $rs['name'],                     
                        'url'   => $rs['url']
                ));
            }
    
            echo Zend_Json::encode($rt); exit;
        }
        else
        {
            echo Zend_Json::encode(array()); exit;
        }
    }
    public function myresizeAction(){
        echo json_encode('value');die;
        $url = $this->_getParam('url', '');
        $width = $this->_getParam('width', 0);
        $height = $this->_getParam('height', 0);

        $url = trim($url);
        $width = intval($width);
        $height = intval($height);

        if($url == '' || $width <= 0 || $height <= 0)
        {
            $arr = array('error' => 6, 'msg' => $this->errorMessage(6));
            echo Zend_Json::encode($arr); exit;
        }
        /*if(strpos($url, '.edn.vn') === false)
        {
            $arr = array('error' => 6, 'msg' => $this->errorMessage(6));
            echo Zend_Json::encode($arr); exit;
        }*/

        $upload = new Utility_Upload();

        $rs = $upload->resizeMyImage($url, $width, $height);
        echo Zend_Json::encode($rs);
        exit;
    }
    
    public function resizeAction()
    {
        $url = $this->_getParam('url', '');
        $width = $this->_getParam('width', 0);
        $height = $this->_getParam('height', 0);
        $exactDimention = $this->_getParam('dimention', 0);
        $background = $this->_getParam('background', 'white');
        $crop = $this->_getParam('crop', 0);
        $quality = $this->_getParam('quality', 80);
         
        $url = trim($url);
        $width = intval($width);
        $height = intval($height);
         
        if($url == '' || $width <= 0 || $height <= 0)
        {
            $arr = array('error' => 6, 'msg' => $this->errorMessage(6));
            echo Zend_Json::encode($arr); exit;
        }
    
        /*if(strpos($url, '.edn.vn') === false)
        {
            $arr = array('error' => 6, 'msg' => $this->errorMessage(6));
            echo Zend_Json::encode($arr); exit;
        }*/
         
        $upload = new Utility_Upload();
         
        $rs = $upload->resizeMobile($url, $width, $height, $crop, $exactDimention, $background, $quality);
         
        if($rs['error_code'] == 0)
        {
            $arr = array('error' => 0, 'src' => $rs['src']);
            echo Zend_Json::encode($arr); exit;
        }
         
        $arr = array('error' => $rs['error_code'], 'msg' => 'System error');
        echo Zend_Json::encode($arr);
        exit;
    }
    
    private function errorMessage($code)
    {
        $errorMessages = array(
                '0' => 'Upload thành công',
                '1' => 'File không hợp lệ',
                '2' => 'Kích thước ảnh vượt qua kích thước cho phép',
                '3' => 'Kích thước ảnh quả nhỏ',
                '4' => 'File không hợp lệ',
                '5' => 'Xảy ra lỗi ra trong quá trình upload (5)',
                '6' => 'Dữ liệu không hợp lệ',
                '7' => 'Xảy ra lỗi ra trong quá trình upload (7)',
                '8' => 'Xảy ra lỗi ra trong quá trình upload (8)',
        );
        
        return isset($errorMessages[$code]) ? $errorMessages[$code] : '';
    }

     public  function upload123go($atomicResize=false, $checkSign=true)
    {
        $upload = isset($_FILES['files']) ? $_FILES['files'] : null;

        $signKey = isset($_POST['signkey']) ? $_POST['signkey'] : '';
        $customName = isset($_GET['file_name']) ? $_GET['file_name'] : '';
        $time = isset($_POST['time']) ? $_POST['time'] : 0;
    
        // validate
        if($checkSign && !$this->verifySignKey($signKey, $time))
        {
            /*return array(
             'error_code' => 6,
            );*/
        }
    
        $info = array();
        if ($upload && is_array($upload['tmp_name'])) {
            // param_name is an array identifier like "files[]",
            // $_FILES is a multi-dimensional array:
            foreach ($upload['tmp_name'] as $index => $value) {
                $fileData = array(
                        'tmp_name'  => $upload['tmp_name'][$index],
                        'error'     => $upload['error'][$index],
                        'name'      => isset($_SERVER['HTTP_X_FILE_NAME']) ? $_SERVER['HTTP_X_FILE_NAME'] : $upload['name'][$index],
                        'size'      => isset($_SERVER['HTTP_X_FILE_SIZE']) ? $_SERVER['HTTP_X_FILE_SIZE'] : $upload['size'][$index],
                        'type'      => isset($_SERVER['HTTP_X_FILE_TYPE']) ? $_SERVER['HTTP_X_FILE_TYPE'] : $upload['type'][$index]
                );

                $destFolder = array('upload', date('Y'), date('m'));
                $rs = $this->handleFileUpload($fileData, $destFolder, $customName);

                $info[] = $rs;
            }
        } 
        elseif ($upload || isset($_SERVER['HTTP_X_FILE_NAME']))
        {
            // param_name is a single object identifier like "file",
            // $_FILES is a one-dimensional array:

            $destFolder = array('upload', date('Y'), date('m'));
            $rs = $this->handleFileUpload($upload, $destFolder);
            $info[] = $rs;
        }
        return $info;
    }

        /**
     * Handle process upload file to server
     * @param array $fileData
     * @param array $rootFolder
     * @return multitype:number |multitype:number unknown |multitype:string unknown multitype:
     */
    public function handleFileUpload($fileData, $destFolders=array(), $customName='')
    {
        if(!$fileData)
        {
            return array('error_code' => 1);
        }
        try
        {
            // neu upload thanh cong
            if($fileData['error'] === UPLOAD_ERR_OK)
            {
                $fileSize = $fileData['size'];
                
                $fileError = $fileData['error'];
                $fileName = $fileData['name'];
                $fileType = $fileData['type'];
                $arrExt     = explode('.', trim($fileName));
                $this->extenion     = '.'. strtolower($arrExt[count($arrExt)-1]);
                
                if(!in_array($this->extenion, array('.jpg', '.gif', '.png')))
                {
                    $this->extenion = '.jpg';
                }
                
                array_pop($arrExt);
                $photoName  = implode(' ', $arrExt);
                
                if($fileName == '')
                    return array('error_code' => 1, 'name' => $fileName, 'size' => $fileSize);
                
                $imageSize = getimagesize($fileData['tmp_name']);
                
                if(!$imageSize)
                    return array('error_code' => 4, 'name' => $fileName, 'size' => $fileSize);
                    
                if($fileSize > $this->max_filesize_upload)
                    return array('error_code' => 2, 'name' => $fileName, 'size' => $fileSize);
                list($width, $height, $type, $attr) = $imageSize;
                
                if($width < $this->min_width && $height < $this->min_height)
                    return array('error_code' => 3, 'name' => $fileName, 'size' => $fileSize);

                $systemName = $customName;
                
                if($systemName == '')
                {
                    $systemName = time().rand();
                    $systemName = md5($systemName);
                }
                else
                {
                    $systemName = My_Zend_Globals::aliasCreator($systemName);
                    $rand = md5(uniqid('123mua'));
                    $systemName .= '-'. substr($rand, 0, 6) .'-'. time();
                }
                
                $folders = array();
    
                // init dest folders
                if(is_array($destFolders) && !empty($destFolders))
                {
                    $destFolders = array_values($destFolders);
                    
                    foreach($destFolders as $key => $folder)
                    {
                        $folders[$key] = $folder;
                    }
                }
                else
                {               
                    $folders[0] = date('Y');
                    $folders[1] = strtolower($systemName[0]);
                    $folders[2] = strtolower($systemName[1]);
                }
                
                // check folder exist
                if(!$this->checkSystemFolder($folders))
                {                   
                    return array('error_code' => 5, 'name' => $fileName, 'size' => $fileSize);
                }
                
                $folder = '';
                
                foreach($folders as $tmp)
                {
                    $folder .= $tmp .'/';
                }
                
                $folder = rtrim($folder, '/');          
                
                $uploadTo = $this->upload_path ."/". $folder ."/". $systemName . $this->extenion;

                
                // upload to original file
                umask(022);
                
                if(move_uploaded_file($fileData['tmp_name'], $uploadTo))
                {
                    
                    return array(           "name"      =>  $photoName,
                                            "url"       =>  $this->publicDomain .'/'. $folder .'/'. $systemName . $this->extenion,                                  
                                            "path"      =>  $folder,
                                            "sys_name"  =>  $systemName,
                                            "ext"       =>  $this->extenion,
                                            "w"         =>  $width,
                                            "h"         =>  $height,
                                            "size"      =>  $fileSize,
                                            "type"      =>  $fileType
                    );
                }
                else
                {                   
                    //My_Zend_Logger::log('Upload::handleFileUpload[8] - Cannot move file from '. $fileData['tmp_name'] .' to '. $uploadTo);
                    return array('error_code' => 8, 'name' => $fileName, 'size' => $fileSize);
                }
            }
            
            return array('error_code' => 7);
        }
        catch(Exception $ex)
        {
           // My_Zend_Logger::log('Upload::handleFileUpload - '. $ex->getMessage());
            return array('error_code' => -7);
        }
    }

    private function checkSystemFolder($folderName)
    {
        try
        {
            $rs = false;
            
            umask(002);
            
            if(is_array($folderName))
            {
                $path = $this->upload_path;
                foreach ($folderName as $folder){
                    $path .= '/'. $folder;
                    if(!is_dir($path))
                    {
                        mkdir($path, 0777, true);    
                    }
                }
                
                $rs = true;
            }
            elseif(!is_dir($this->upload_path .'/'. $folderName))
            {
                $rs = mkdir($this->upload_path .'/'. $folderName, 0777, true);
            }
            else 
            {
                $rs = true;
            }
            
            return $rs;
        }
        catch(Exception $ex)
        {
            //My_Zend_Logger::log($ex->getMessage());
            return false;
        }
    }

    public function resizeMobile($url, $width, $height, $crop = false, $exactDimentions = true, $background = 'white', $quality = 80)
    {
        $parts = parse_url($url);
        
        $path = $parts['path'];
        
        $path = ltrim($path, '/');
        
        $uri = explode('/', $path);
        
        $fileName = end($uri);
        
        if($width < $this->min_width || $height < $this->min_height)
        {
            return array('error_code' => 3, 'name' => $fileName);
        }
        
        if($fileName == '' || strpos($fileName, '.') == false)
        {
            return array('error_code' => 3, 'name' => $fileName);
        }
        
        $filePath = $this->upload_path .'/'. $path;
        
        if(!file_exists($filePath))
        {
            return array('error_code' => 4, 'msg' => 'File is not exist');
        }
        
        $destFolders = str_replace('/'. $fileName, '', $path);
        $destFolders = explode('/', $destFolders);
    
        $folder = 'mobile';
        
        array_unshift($destFolders, $folder);
                
        try
        {
            // check folder exist
            if(!$this->checkSystemFolder($destFolders))
                return array('error_code' => 5, 'name' => $fileName, 'size' => $fileSize);
    
            $destFolders = implode('/', $destFolders);
            $destFile = explode('.', $fileName);
            
            $url = $this->publicDomain .'/'. $destFolders .'/'. $destFile[0] .'_'. $width .'x'. $height .'.'. $destFile[1];     
            $destFile = $this->upload_path .'/'. $destFolders .'/'. $destFile[0] .'_'. $width .'x'. $height .'.'. $destFile[1];                          
            if($crop)
            {
                $tmpWidth = $width;
                
                if($width < $height)
                {
                    $tmpWidth = $height;                    
                }
                
                $rs = $this->crop($filePath, $destFile, $width, $height, $quality);             
            }
            else
            {               
                $rs = $this->resize($filePath, $destFile, $width, $height, $exactDimentions, $background, $quality);
            }
            
            if($rs)
            {           
                return array('error_code' => 0, 'src' => $url);
            }
            
            return array('error_code' => 1, 'msg' => 'System busy');
        }
        catch(Exception $ex)
        {
            return array('error_code' => 7, 'msg' => $ex->getMessage());
        }
    }

    function resize($source, $destFile, $width = 1024, $height = 1024, $exactDimentions = false, $background='', $quality = 80)
    {
        $phMagick = new phMagick($source, $destFile);
        
        $phMagick->setImageQuality($quality);
        
        return $phMagick->resize($width, $height, $exactDimentions, $background);
    }
    
    function crop($source, $destFile, $width = 1024, $height = 1024, $quality = 100)
    {
        $phMagick = new phMagick($source, $destFile);
        
        $phMagick->setImageQuality($quality);
        
        return $phMagick->mycrop($width, $height);
    }


    /**
    * Resize an image and keep the proportions
    * @author Allison Beckwith <allison@planetargon.com>
    * @param string $filename
    * @param integer $max_width
    * @param integer $max_height
    * @return image
    */
    public  function resizeMyImage($file, $w, $h) {
        //Get the original image dimensions + type
        list($source_width, $source_height, $source_type) = getimagesize($file);

        $parts = parse_url($file);
        $path = $parts['path'];
        $path = ltrim($path, '/');
        $uri = explode('/', $path);
        $fileName = explode('.', end($uri))[0];
        //Figure out if we need to create a new JPG, PNG or GIF
        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        if ($ext == "jpg" || $ext == "jpeg") {
            $source_gdim=imagecreatefromjpeg($file);
        } elseif ($ext == "png") {
            $source_gdim=imagecreatefrompng($file);
        } elseif ($ext == "gif") {
            $source_gdim=imagecreatefromgif($file);
        } else {
            //Invalid file type? Return.
            return;
        }
     
        //If a width is supplied, but height is false, then we need to resize by width instead of cropping
        if ($w && !$h) {
            $ratio = $w / $source_width;
            $temp_width = $w;
            $temp_height = $source_height * $ratio;
     
            $desired_gdim = imagecreatetruecolor($temp_width, $temp_height);
            imagecopyresampled(
                $desired_gdim,
                $source_gdim,
                0, 0,
                0, 0,
                $temp_width, $temp_height,
                $source_width, $source_height
            );
        } else {
            $source_aspect_ratio = $source_width / $source_height;
            $desired_aspect_ratio = $w / $h;
            if ($source_aspect_ratio > $desired_aspect_ratio) {
                /*
                 * Triggered when source image is wider
                 */
                $temp_height = $h;
                $temp_width = ( int ) ($h * $source_aspect_ratio);
            } else {
                /*
                 * Triggered otherwise (i.e. source image is similar or taller)
                 */
                $temp_width = $w;
                $temp_height = ( int ) ($w / $source_aspect_ratio);
            }
            /*
             * Resize the image into a temporary GD image
             */
            $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
            imagecopyresampled(
                $temp_gdim,
                $source_gdim,
                0, 0,
                0, 0,
                $temp_width, $temp_height,
                $source_width, $source_height
            );
            /*
             * Copy cropped region from temporary image into the desired GD image
             */
            $x0 = ($temp_width - $w) / 2;
            $y0 = ($temp_height - $h) / 2;
            $desired_gdim = imagecreatetruecolor($w, $h);
            imagecopy(
                $desired_gdim,
                $temp_gdim,
                0, 0,
                $x0, $y0,
                $w, $h
            );
        }

        //Domain


        $folder = date('Y').'/'.date('m');
        /*
         * Render the image
         * Alternatively, you can save the image in file-system or database
         */
        $destination =  $this->upload_path.'/upload/' .$folder .'/'. $fileName .'_'. $w .'x'. $h .'.'. $ext;
        $url = $this->publicDomain.'/upload/'.$folder .'/'. $fileName .'_'. $w .'x'. $h .'.'. $ext;
        if ($ext == "jpg" || $ext == "jpeg") {
            ImageJpeg($desired_gdim,$destination,100);
        } elseif ($ext == "png") {
            ImagePng($desired_gdim,$destination);
        } elseif ($ext == "gif") {
            ImageGif($desired_gdim,$destination);
        } else {
            return array('error_code' => 1, 'msg' => 'System busy');
        }
        ImageDestroy ($desired_gdim);

        return array('error_code' => 0, 'src' => $url);

    }
}
