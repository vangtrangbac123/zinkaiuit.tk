<?php

class Admin_IndexController extends My_Controller_Action {
    public function init() {

        $this->auth = Zend_Auth::getInstance();
        if (!$this->auth->hasIdentity()) {
            $this->_redirect('/admin/auth');
        }
    }

    public function reportAction() {
        $yId = 0;
        $youtube = $this->model->Youtube->getYoutube();

        if($youtube){
            $yId = $youtube->youtube_id;
        }
        $list_user = $this->model->User->getList($yId);

        $this->view->youtube = $youtube;
        $this->view->list_user = $list_user;
        $this->_helper->viewRenderer("index");
    }

    public function indexAction(){
        $params = $this->getRequest()->getParams();

        $where = "";

        if(isset($this->user->user_id)){
             $where  = "AND account = ".$this->user->user_id;
        }

        $sql ="SELECT
                    youtube_id,
                    link,
                    title,
                    is_active,
                    is_report,
                    (CASE type_id
                        WHEN 0 THEN 'Misleading text'
                        WHEN 1 THEN 'Advertise'
                        WHEN 2 THEN 'Thumbnail'
                        WHEN 3 THEN 'Scams'
                        END
                    ) type_id
                FROM report_link rl
                WHERE  is_active = 1 AND source_id = 0 $where
                ORDER BY rl.order DESC, FIELD(is_report,2,0,1)";
        $list_youtube = $this->model->Userreport->getRows($sql);


        $list_showing = new stdClass();
        if($list_youtube && is_array($list_youtube)){
            $youtube_id = $list_youtube{0}->youtube_id;
            if(isset($params['yId'])){
                $youtube_id = $params['yId'];
            }
            $sql ="SELECT
                    ur.email,
                    ur.pass,
                    url.is_report,
                    rt.report_text
                FROM user_report_link url
                JOIN user_report ur ON ur.user_id = url.user_id
                LEFT JOIN report_text rt ON rt.report_id = url.report_text_id
                WHERE
                    url.youtube_id = $youtube_id
                    AND ur.is_active = 1
                    AND rt.report_text is not null
            ";
            $list_showing = $this->model->Userreport->getRows($sql);
        }

        $this->view->list_youtube = $list_youtube;
        $this->view->list_showing = $list_showing;
        $this->_helper->viewRenderer("report");

    }

    public function likeAction(){
        $params = $this->getRequest()->getParams();

        $where = "";

        if(isset($this->user->user_id)){
             $where  = "AND account = ".$this->user->user_id;
        }

        $sql ="SELECT
                    youtube_id,
                    link,
                    title,
                    is_active,
                    is_report
                FROM report_link rl
                WHERE  is_active = 1 AND source_id = 1 $where
                ORDER BY rl.order DESC, FIELD(is_report,2,0,1)";
        $list_youtube = $this->model->Userreport->getRows($sql);


        $list_showing = new stdClass();
        if($list_youtube && is_array($list_youtube)){
            $youtube_id = $list_youtube{0}->youtube_id;
            if(isset($params['yId'])){
                $youtube_id = $params['yId'];
            }
            $sql ="SELECT
                    ur.email,
                    ur.pass,
                    url.is_report,
                    rt.report_text
                FROM user_report_link url
                JOIN user_report ur ON ur.user_id = url.user_id
                LEFT JOIN report_text rt ON rt.report_id = url.report_text_id
                WHERE
                    url.youtube_id = $youtube_id
                    AND ur.is_active = 1
                    AND rt.report_text is not null
            ";
            $list_showing = $this->model->Userreport->getRows($sql);
        }

        $this->view->list_youtube = $list_youtube;
        $this->view->list_showing = $list_showing;

    }


}