<?php

class Admin_Model_Youtube extends My_Model_Abstract {

    protected $_name    = 'youtube';
    protected $_primary = 'youtube_id';

    public function getYoutube(){
    	$sql = "SELECT * FROM youtube WHERE is_active = 1 ORDER BY date_add DESC";
    	return $this->_db->fetchRow($sql);
    }


    public function getYoutubeOne(){
        $sql = "SELECT link FROM youtube WHERE is_active = 1 ORDER BY date_add DESC";
        return $this->_db->fetchOne($sql);
    }

}