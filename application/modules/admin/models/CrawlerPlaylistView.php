<?php

class Admin_Model_CrawlerPlaylistView extends My_Model_Abstract {

    protected $_name    = 'crawler_playlist_view';
    protected $_primary = 'text_id';

}