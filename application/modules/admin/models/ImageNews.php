<?php

class Admin_Model_ImageNews extends My_Model_Abstract {

    protected $_name    = 'news_image';
    protected $_primary = 'news_id';

    public function check($params){
       $sql = 'SELECT image_id
               FROM   news_image
               WHERE  type_id =:type_id AND news_id =:news_id AND is_active = 1
               ORDER BY is_active DESC,image_id,date_add
               LIMIT 1';
       return $this->_db->fetchOne($sql,array('type_id' => $params['type_id'],'news_id' => $params['news_id']));
    }
}