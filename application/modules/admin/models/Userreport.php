<?php

class Admin_Model_Userreport extends My_Model_Abstract {

    protected $_name    = 'user_report';
    protected $_primary = 'user_id';

    public function deleteTags($data){
    	return $this->_db->delete($data['table'],"youtube_id = ".$data['youtube_id']);
    }

    public function getUserReport($account, $sourceId, $is_subcribe ){
        $where = "";
        $where .= " AND rl.source_id =".$sourceId;
        $where .=" AND rl.account =".$account;
        $where .=" AND rl.is_subcribe =".$is_subcribe;
    	$sql ="SELECT
					ur.email,
					ur.pass,
					rl.link,
                    rl.type_id,
                    rl.like_number,
                    rl.share_number,
                    rl.comment_number,
                    rl.subcribe_number,
					url.user_id,
					url.youtube_id,
                    rt.report_text
				FROM user_report_link url
				JOIN user_report ur ON ur.user_id = url.user_id
				JOIN report_link rl ON rl.youtube_id = url.youtube_id
                LEFT JOIN report_text rt ON rt.report_id = url.report_text_id
				WHERE url.is_report = 0
					AND rl.is_report <> 1
					AND rl.is_active = 1
					AND ur.is_active = 1
                    AND rt.report_text is not null
                    $where
				ORDER BY rl.order DESC, rl.youtube_id ASC
				LIMIT 1";
		$r = $this->_db->fetchRow($sql);
		if($r){
				$this->_db->update('user_report_link',
    	 					array('is_report' => 2),
    	 					array('youtube_id = ?'  =>  $r->youtube_id, 'user_id = ?' => $r->user_id)
		    	 			);
                $this->_db->update('report_link',
                            array('is_report' => 2),
                            array('youtube_id = ?'  =>  $r->youtube_id)
                            );
		}
		return $r;
    }

    public function getReportTxt(){
    	$sql ="SELECT report_text FROM report_text WHERE is_active = 1 ORDER BY RAND()";
    	return $this->_db->fetchOne($sql);
    }

    public function getAllReport($typeId = 0){
        $sql ="SELECT report_id FROM report_text WHERE is_active = 1 and type_id = $typeId ORDER BY RAND()";
        return $this->_db->fetchAll($sql);
    }

    public function getOneReport($typeId = 0){
        $sql ="SELECT report_id FROM report_text WHERE is_active = 1 and type_id = $typeId ORDER BY RAND()";
        return $this->_db->fetchOne($sql);
    }

    public function updateReport($data){
    	 $this->_db->update('user_report_link',
    	 					array('is_report' => 1, 'date_active' => date('Y-m-d H:i:s')),
    	 					array('user_id  = ?'  => $data['u'],
		                         'youtube_id = ?'  =>  $data['y'])
		    	 			);
    	 $sql ="SELECT COUNT(*) FROM user_report_link WHERE youtube_id = :youtube_id AND is_report = 0";
    	 $count = $this->_db->fetchOne($sql,array('youtube_id' => $data['y']));
    	 if($count ==0){
    	 	$this->_db->update('report_link',
    	 					array('is_report' => 1),
    	 					array('youtube_id = ?'  =>  $data['y'])
		    	 			);
    	 }
         if(isset($data['l'])){
            $this->_db->update('report_link',
                            array('like_number' => $data['l'],
                                 'share_number' => $data['s'],
                                 'comment_number' => $data['c'],
                                 'subcribe_number' => $data['sub']
                                ),
                            array('youtube_id = ?'  =>  $data['y'])
                            );
         }
    }

    public function updateEmailTerminate($data){
        $this->_db->update('user_report',
                            array('fullname' => 'del'),
                            array('email  = ?'  => $data['e'])
                            );
    }

}