<?php

class Admin_Model_Plsshow extends My_Model_Abstract {

    protected $_name    = 'playlist_show';
    protected $_primary = 'show_id';

    public function getListVideo($email){
    $email = Utility_Validate::trimEmail($email);
    $sql = "SELECT
                    list_add,
                    list_remove,
                    is_delete
                FROM playlist_show
                WHERE is_active = 1
                    AND list_email like '%$email%'";
    return  $this->_db->fetchRow($sql, array('email' => $email));

  }

}