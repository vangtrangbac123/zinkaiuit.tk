<?php

class Admin_Model_Crawler extends My_Model_Abstract {

    protected $_name    = 'crawler_text_search';
    protected $_primary = 'text_id';

 public function getVideo($user_id, $email){
 	$email = Utility_Validate::trimEmail($email);
	$sql = "SELECT
    				v.video_id,
    				tx.videos_position,
                    tx.text_value text_search,
                    v.text_id
    			FROM crawler_text_search  tx
    			JOIN crawler_videos_add v  ON v.text_id = tx.text_id
    			WHERE tx.is_active = 1
    				AND tx.email= '$email'
    				AND tx.user_id = $user_id
    				AND v.status_id = 1
    			ORDER BY length(v.view) DESC, v.view DESC";
    	$r =  $this->_db->fetchRow($sql, array('email' => $email, 'user_id' => $user_id));

    	if($r){
				$this->_db->update('crawler_videos_add',
    	 					array('status_id' => 2),
    	 					array('video_id = ?'  =>  $r->video_id, 'text_id = ?'  =>  $r->text_id)
		    	 			);
			}
		return $r;
  }
  public function updateStatus($data){
  	$this->_db->update('crawler_videos_add',
    	 					array('status_id' => 3),
    	 					array('video_id = ?'  =>  $data['video_id'],
                                  'text_id = ?'   =>  $data['text_id'])
		    	 			);
  }

  public function updateLog($data){
    $data['email'] = Utility_Validate::trimEmail($data['email']);
    $sql = "INSERT INTO
                crawler_log (stt, email, text_search, date_add)
                VALUES ( '', '".$data['email']."', '".$data['text_search']."', '". date("Y-m-d H:i:s")."')";
                //var_dump($sql);die;
    return $this->_db->query($sql);
  }

  

}