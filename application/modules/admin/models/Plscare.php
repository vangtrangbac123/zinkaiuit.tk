<?php

class Admin_Model_Plscare extends My_Model_Abstract {

    protected $_name    = 'playlist_care';
    protected $_primary = 'v_id';

    public function getVideo($email){
    $email = Utility_Validate::trimEmail($email);
    $sql = "SELECT
                    v_id,
                    video_id,
                    videos_position,
                    status_id
                FROM playlist_care
                WHERE is_active = 1
                    AND status_id =1
                    AND list_email like '%$email%'";
    $r =  $this->_db->fetchRow($sql, array('email' => $email));
    if($r){
                $this->_db->update('playlist_care',
                            array('status_id' => 2),
                            array('v_id = ?'  =>  $r->v_id)
                            );
    }
    return $r;

  }
  public function updateStatus($data){
    $this->_db->update('playlist_care',
                            array('status_id' => 3),
                            array('v_id = ?'  =>  $data['v_id'])
                            );
  }

}