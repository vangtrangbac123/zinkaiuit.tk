<?php

class Admin_Model_Comment extends My_Model_Abstract {

    protected $_name    = 'comment';
    protected $_primary = 'comment_id';

    public function getComment($data){
    	$sql ="SELECT comment_id FROM comment WHERE user_id = :user_id AND youtube_id = :youtube_id";
    	return $this->_db->fetchOne($sql, array('user_id' => $data['user_id'], 'youtube_id' => $data['youtube_id']));
    }

}