            <!-- start: sidebar -->
            <aside id="sidebar-left" class="sidebar-left">

                <div class="sidebar-header">
                     <div class="sidebar-title">
                        <?php echo $this->user->fullname?> | <a href="/auth/logout/" title="Sign Out">Sign Out</a>
                    </div>
                    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
                        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
                    </div>
                </div>

                <div class="nano">
                    <div class="nano-content">
                        <nav id="menu" class="nav-main" role="navigation">
                            <ul class="nav nav-main">
                                <li data-controller="index">
                                    <a href="/index">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <span>Dashboard Auto report</span>
                                    </a>
                                </li>
                                <li data-controller="index">
                                    <a href="/index/like">
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <span>Dashboard Auto like</span>
                                    </a>
                                </li>
                                 <li data-controller="crawler" class="nav-parent">
                                    <a>
                                        <i class="fa fa-home" aria-hidden="true"></i>
                                        <span>Raw data</span>
                                    </a>
                                     <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/crawler/detail/">Add </a>
                                        </li>
                                         <li data-action="index">
                                         <a href="/admin/crawler/">List</a>
                                        </li>
                                        <li data-action="index">
                                         <a href="/admin/plsshow/">Show</a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="nav-parent" data-controller="payment">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>Payment </span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/payment/detail/">Add </a>
                                        </li>
                                         <li data-action="index">
                                            <a href="/admin/payment/">Payment</a>
                                        </li>
                                    </ul>
                                </li> 

                                <li class="nav-parent" data-controller="chanel">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>Chanel </span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/chanel/detail/">Add </a>
                                        </li>
                                         <li data-action="index">
                                            <a href="/admin/chanel/">Chanel</a>
                                        </li>
                                    </ul>
                                </li> 

                                <li class="nav-parent" data-controller="youtube">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>Youtube auto view</span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/youtube/detail/">Add Youtube </a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/youtube/">Youtube</a>
                                        </li>
                                    </ul>
                                </li>
                                <h6>Report</h6>
                                <li class="nav-parent" data-controller="userreport">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>List  Email</span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/userreport/detail/">Add </a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/userreport/">List</a>
                                        </li>
                                    </ul>
                                </li> 
                                 <li class="nav-parent" data-controller="reportlink">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>List Link</span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/reportlink/detail/">Add </a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/reportlink/">Misleading Text</a>
                                        </li>
                                        <li data-action="advertise">
                                            <a href="/admin/reportlink/advertise">Advertise</a>
                                        </li>
                                        <li data-action="thumbnail">
                                            <a href="/admin/reportlink/thumbnail">Thumbnail</a>
                                        </li>
                                        <li data-action="scams">
                                            <a href="/admin/reportlink/scams">Scams</a>
                                        </li>
                                    </ul>
                                </li> 
                                 <li class="nav-parent" data-controller="reporttext">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>List Text</span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/reporttext/detail/">Add </a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/reporttext/">Misleading Text</a>
                                        </li>
                                        <li data-action="advertise">
                                            <a href="/admin/reporttext/advertise">Advertise</a>
                                        </li>
                                        <li data-action="thumbnail">
                                            <a href="/admin/reporttext/thumbnail">Thumbnail</a>
                                        </li>
                                        <li data-action="scams">
                                            <a href="/admin/reporttext/scams">Scams</a>
                                        </li>
                                    </ul>
                                </li>
                            <h6>Like share comment</h6>
                                <li class="nav-parent" data-controller="userlike">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>List  Email</span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/userlike/detail/">Add </a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/userlike/">List</a>
                                        </li>
                                    </ul>
                                </li> 
                                 <li class="nav-parent" data-controller="likelink">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>List Link</span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/likelink/detail/">Add </a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/likelink/"> List</a>
                                        </li>
                                    </ul>
                                </li> 
                                 <li class="nav-parent" data-controller="commenttext">
                                    <a>
                                        <i class="fa fa-cube" aria-hidden="true"></i>
                                        <span>List Text</span>
                                    </a>
                                    <ul class="nav nav-children">
                                       <li data-action="detail">
                                            <a href="/admin/commenttext/detail/">Add </a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/commenttext/static">Static</a>
                                        </li>
                                        <li data-action="index">
                                            <a href="/admin/commenttext/dynamic">Dynamic</a>
                                        </li>
                                    </ul>
                                </li> 



                            </ul>
                        </nav>
                        <br>
                        <div class="sidebar-widget widget-tasks">
                            <div class="widget-header">
                                <h6>Others</h6>
                            </div>
                            <div class="widget-content">
                                <ul class="list-unstyled m-none">
                                    <li><a href="#">Clear cache</a></li>
                                    <li><a href="#">Old version</a></li>
                                </ul>
                            </div>
                        </div>

                    </div>

                </div>

            </aside>
            <!-- end: sidebar -->