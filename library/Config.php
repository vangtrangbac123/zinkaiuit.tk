<?php

require APPLICATION_PATH . '/configs/config.php';

if (file_exists(APPLICATION_PATH . '/configs/' . APPLICATION_ENV . '/config.php')) {
    require APPLICATION_PATH . '/configs/' . APPLICATION_ENV . '/config.php';
}

if (!class_exists('Config')) {
    class Config extends Config_Default {};
}
