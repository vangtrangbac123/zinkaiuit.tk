<?php 
class Google_Api {
	//Delete Playlist
   public static function playlistsDelete($service, $id, $params) {
	    $params = array_filter($params);
	    $response = $service->playlists->delete(
	        $id,
	        $params
	    );

	    return $response;
	}

	// Sample php code for playlists.list

    public static function playlistsListByChannelId($service, $part, $params) {
        $params = array_filter($params);
        $response = $service->playlists->listPlaylists(
            $part,
            $params
        );

        return $response;
    }
    //// Sample php code for playlistItems.list
    public static function playlistItemsListByPlaylistId($service, $part, $params) {
        $params = array_filter($params);
        $response = $service->playlistItems->listPlaylistItems(
            $part,
            $params
        );

        return $response;
    }

    public static function createNewPlaylist($service, $playlist_title, $playlist_description = ""){

        // 1. Create the snippet for the playlist. Set its title and description.
        $playlistSnippet = new Google_Service_YouTube_PlaylistSnippet();
        $playlistSnippet->setTitle($playlist_title);
        $playlistSnippet->setDescription($playlist_description);

        // 2. Define the playlist's status.
        $playlistStatus = new Google_Service_YouTube_PlaylistStatus();
        $playlistStatus->setPrivacyStatus('public');

        // 3. Define a playlist resource and associate the snippet and status
        // defined above with that resource.
        $youTubePlaylist = new Google_Service_YouTube_Playlist();
        $youTubePlaylist->setSnippet($playlistSnippet);
        $youTubePlaylist->setStatus($playlistStatus);

        // 4. Call the playlists.insert method to create the playlist. The API
        // response will contain information about the new playlist.
        $playlistResponse = $service->playlists->insert('snippet,status',
            $youTubePlaylist, array());
        return $playlistResponse['id'];

    }

    public static function addVideo($service, $playlistId, $videoId, $position = false){
        // 5. Add a video to the playlist. First, define the resource being added
        // to the playlist by setting its video ID and kind.
        $resourceId = new Google_Service_YouTube_ResourceId();
        $resourceId->setVideoId($videoId);
        $resourceId->setKind('youtube#video');

        // Then define a snippet for the playlist item. Set the playlist item's
        // title if you want to display a different value than the title of the
        // video being added. Add the resource ID and the playlist ID retrieved
        // in step 4 to the snippet as well.
        $playlistItemSnippet = new Google_Service_YouTube_PlaylistItemSnippet();
        $playlistItemSnippet->setPlaylistId($playlistId);
        $playlistItemSnippet->setResourceId($resourceId);

        if($position)
        $playlistItemSnippet->setPosition($position);

        // Finally, create a playlistItem resource and add the snippet to the
        // resource, then call the playlistItems.insert method to add the playlist
        // item.
        $playlistItem = new Google_Service_YouTube_PlaylistItem();
        $playlistItem->setSnippet($playlistItemSnippet);
        $playlistItemResponse = $service->playlistItems->insert(
            'snippet,contentDetails', $playlistItem, array());
        return $playlistItemResponse;
    }

    public static function searchListByKeyword($service, $part, $params) {
    $params = array_filter($params);
    $response = $service->search->listSearch(
        $part,
        $params
    );

    return $response;
}

   



}
