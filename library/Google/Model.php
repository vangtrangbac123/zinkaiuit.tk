<?php

class Web_PlaylistController extends My_Controller_Action {

    public function init() {
    }

    public function testAction(){
        $this->updateExpiredToken(3);
        $service = new Google_Service_YouTube($this->client);
        //By Channel ID
        $playlist = $this->playlistsListByChannelId($service,
            'snippet,contentDetails', 
            array('channelId' => 'UC_x5XG1OV2P6uZZ5FSM9Ttw', 'maxResults' => 25));
        //By Mýelf
        $mine = $this->playlistsListByChannelId($service,
        'snippet,contentDetails', 
        array('mine' => true, 'maxResults' => 25, 'onBehalfOfContentOwner' => '', 'onBehalfOfContentOwnerChannel' => ''));

        var_dump($mine);die;
    }

    public function playlistitemAction(){
        $this->updateExpiredToken(3);
        $service = new Google_Service_YouTube($this->client);
        $list = $this->playlistItemsListByPlaylistId($service,
        'snippet,contentDetails', 
        array('maxResults' => 25, 'playlistId' => 'PLBCF2DAC6FFB574DE'));
        var_dump($list);die;
        }

    public function indexAction() {
        $params = $this->getRequest()->getParams();

        if (isset($params['code'])) {
          // //$accessToken = $client->authenticate($params['code']);
          //   $accessToken = array(
          //       'access_token' => 'ya29.GlvPBHd7Fgio9YH_Hr2T6noIKjieTMpCdRCXV9cbycpuziwspFCIZhvJJcfOnroFfKwZ7PG6emZ-TcelPJyqEPBNAIBHCIcl8G_WCQ6-uiASvIOtdIXlb06Vw-AN',
          //       'token_type' => 'Bearer',
          //       'expires_in' => 3600,
          //       'refresh_token' => '1/c2qnW3nWbeI8SgPB232Mt036fvCyo34k8mxQCFQ72hA',
          //       'created' => 1506172321
          //   );
          //   $accessToken['date_add'] = date("Y-m-d H:i:s");

          // if(isset($accessToken['access_token'])){
          //   $this->model->Google->save($accessToken);
          // }
            $this->insertToken($params['code']);
            var_dump('expression');die;
        }


        // var_dump("Nothing");die;
    }

    public function insertToken($code){
        $this->client = Google_Auth::getconfig($this->client);
        $accessToken = $this->client->authenticate($code);


         if(isset($accessToken['refresh_token'])){
            $channel = $this->getChannelInfo();
            $accessToken['date_add'] = date("Y-m-d H:i:s");
            $accessToken['channel_id'] = $channel['id'];
            $accessToken['channel_name'] = $channel['name'];

            $checkDuplicate = $this->model->Google->_find(array('refresh_token' => $accessToken['refresh_token']));

            if(!$checkDuplicate){
                $access_token['auth_google_id'] = $this->model->Google->save($accessToken);
                $_SESSION['client'] = array(
                    'access_token' => $access_token,
                    'client'       => $this->client
                );
            }
          }
    }
    public function updateExpiredToken($auth_google_id){
        $this->client = Google_Auth::getconfig($this->client);
        //$a = Google_Auth::updateToken($auth_google_id);
         $model = $this->model->Google->get($auth_google_id);

        $this->client->setAccessToken($model->access_token);
        $isExpired = Google_Auth::isAccessTokenExpired($model->created, $model->expires_in);
        //var_dump($isExpired);die;

        if ($isExpired) {
            // $revokeToken = $this->client->revokeToken();
            // var_dump($this->client);die;
            // if(!$revokeToken){
                $update_token = $this->client->refreshToken($model->refresh_token);
                if(isset($update_token['access_token'])){
                    $update_token['auth_google_id'] = $auth_google_id;
                    $this->model->Google->save($update_token);

                     $_SESSION['client'] = array(
                        'access_token' => $update_token,
                        'client'       => $this->client
                    );
            // }else{
            //     echo "Token Revoked By User";die;
            // }
          }
        }
        return $this->model->Google->get($auth_google_id);
    }

    public function getChannelInfo(){
        $youtube = new Google_Service_YouTube($this->client);
        $listResponse = $youtube->channels->listChannels('brandingSettings', array('mine' => true));
        //Channel ID

        $info_channel = array();
        $info_channel['name'] = $listResponse['items'][0]->brandingSettings->channel->title;
        $info_channel['id'] =$listResponse['items'][0]->id;
        return $info_channel;

    }

    // Sample php code for playlists.list

    public function playlistsListByChannelId($service, $part, $params) {
        $params = array_filter($params);
        $response = $service->playlists->listPlaylists(
            $part,
            $params
        );

        return $response;
    }
    //// Sample php code for playlistItems.list
    public function playlistItemsListByPlaylistId($service, $part, $params) {
        $params = array_filter($params);
        $response = $service->playlistItems->listPlaylistItems(
            $part,
            $params
        );

        return $response;
    }



}