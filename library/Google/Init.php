<?php 
class Google_Init{
	  public $client;
	  private $model;

	  public function __construct()
  {
  	$this->client = new Google_Client();
	$this->client->setAuthConfigFile(GOOGLE_LIB.'/client_secret.json');
	// Set to valid redirect URI for your project.
	$this->client->setRedirectUri('http://www.zinkaiuit.ga/playlist');

	$this->client->addScope('https://www.googleapis.com/auth/youtube');
	$this->client->setAccessType('offline');
	$this->model = My_Model_Helperweb::getInstance();
    
  }

  public static function isAccessTokenExpired($created, $expires)
  {

    // If the token is set to expire in the next 30 seconds.
    return ($created + ($expires - 30)) < time();
  }

  public function insertToken($code){
   
        $accessToken = $this->client->authenticate($code);

         if(isset($accessToken['refresh_token'])){
            $channel = $this->getChannelInfo();
            $accessToken['date_add'] = date("Y-m-d H:i:s");
            $accessToken['channel_id'] = $channel['id'];
            $accessToken['channel_name'] = $channel['name'];
            $accessToken['user_id'] = $_SESSION['login']->user_id;

            $checkDuplicate = $this->model->Google->_find(array('refresh_token' => $accessToken['refresh_token']));

            if(!$checkDuplicate){
                $accessToken['auth_google_id'] = $this->model->Google->save($accessToken);

                //SESSION
                $_SESSION['access_token'] = array(
                    'auth_google_id' => $accessToken['auth_google_id'],
                    'channel_id' => $accessToken['channel_id'],
                    'access_token' => $accessToken['access_token'],
                    'expires_in' => $accessToken['expires_in'],
                    'created' => $accessToken['created'],
                );
            }
          }
    }
    public function updateExpiredToken($auth_google_id){
        //$a = Google_Auth::updateToken($auth_google_id);
         $model = $this->model->Google->get($auth_google_id);

        $this->client->setAccessToken($model->access_token);
        $isExpired = self::isAccessTokenExpired($model->created, $model->expires_in);
        //var_dump($isExpired);die;
        $session = array(
        	'auth_google_id' => $auth_google_id,
            'channel_id' => $model->channel_id,
            'access_token' => $model->access_token,
            'expires_in' =>  $model->expires_in,
            'created' => $model->created,
        );

        if ($isExpired) {
            // $revokeToken = $this->client->revokeToken();
            // var_dump($this->client);die;
            // if(!$revokeToken){
                $update_token = $this->client->refreshToken($model->refresh_token);
                // array(2) {
                //   ["error"]=>
                //   string(13) "invalid_grant"
                //   ["error_description"]=>
                //   string(11) "Bad Request"
                // }
                if(isset($update_token['access_token'])){
                    $update_token['auth_google_id'] = $auth_google_id;
                    $this->model->Google->save($update_token);

                    //Update session
                    $session['access_token'] = $update_token['access_token'];
                    $session['expires_in'] = $update_token['expires_in'];
                    $session['created'] = $update_token['created'];

            //     echo "Token Revoked By User";die;
            // }
                }else{return false;}
        }

         //CREATE SESSION
         $_SESSION['access_token'] = $session;

        return $this->client;
    }

    public function getClientBySession($auth_google_id){
    	if(isset($_SESSION['access_token'])){
	    	$isExpired = self::isAccessTokenExpired($_SESSION['access_token']['created'], $_SESSION['access_token']['expires_in']);
	    	if ($isExpired) {
	    		$this->updateExpiredToken($auth_google_id);
	    	}

	    	$this->client->setAccessToken($_SESSION['access_token']['access_token']);

	    }else{
	    	    $this->updateExpiredToken($auth_google_id);
	    }

	    return $this->client;
    }

    public function getChannelInfo(){
        $youtube = new Google_Service_YouTube($this->client);
        $listResponse = $youtube->channels->listChannels('brandingSettings', array('mine' => true));
        //Channel ID

        $info_channel = array();
        $info_channel['name'] = $listResponse['items'][0]->brandingSettings->channel->title;
        $info_channel['id'] =$listResponse['items'][0]->id;
        return $info_channel;

    }




}