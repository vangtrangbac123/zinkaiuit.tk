<?php
require APPLICATION_PATH . '/modules/web/controllers/ajax/simple_html_dom.php';
class My_Crawler_Htmldom {

    public static $html_content = '';
    public static $arr_att_clean = array();
      // function lay title cua bai viet


   static function getVideos($link, $att_title){
        if(self::$html_content==''){
            $html = file_get_html($link);
            self::$html_content = $html;
        }else{
            $html = self::$html_content;
        }
        $count = 0;
        $arr_video = array();

        foreach($html->find($att_title) as $e){
            $link_vids = $e->getElementsByTagName("a",1);
            $info_view = $e->getElementsById(".yt-lockup-meta-info",0);


            $view_string = $info_view->children(1);
            $view = explode(" ",$view_string->plaintext);


            $arr_video[] = array(
                                "video_name" => $link_vids->getAttribute("title"),
                                "video_link" => $link_vids->getAttribute("href"),
                                "video_view" => $view[0],
                            );

        }
        return $arr_video;
    }

     public static function getVideosByWeek($link, $att_title, $keyword){
        if(self::$html_content==''){
            $html = file_get_html($link);
            self::$html_content = $html;
        }else{
            $html = self::$html_content;
        }
        $arr_video = array();

        $html = file_get_html($link);

        $count = 0;
        $arr = array();

        foreach($html->find($att_title) as $e){
            $link_vids = $e->getElementsByTagName("a",1);
            $info_view = $e->getElementsById(".yt-lockup-meta-info",0);


            $view_string = $info_view->children(1);
            if(!isset($view_string->plaintext)) continue;
            $view = explode(" ",$view_string->plaintext);
            if(!isset($view[1])) continue;//Next Quang cao


            $arr_row = array(   "keyword"    => $keyword,
                                "video_name" => $link_vids->getAttribute("title"),
                                "video_id"   => substr($link_vids->getAttribute("href"),9),
                                "video_view" => $view[0],
                            );
            $arr[] = $arr_row;
            //$this->model->insertVideos($arr_row);


        }
        return $arr;
    }

     public static function getVideoToAddPlaylist($keyword, $byDate = 0, $count_view = 6){
            $keyword = str_replace(" ", "+", $keyword);

            $date = array(
                0 => "EgIIA1AU", //Week
                1 => "EgIIAVAU", //Hour
                2 => "EgIIAlAU", //Date
                3 => "EgIIBFAU", //Month
                );

            $arr_view = array(
                0 => 6, //Week
                1 => 4, //Hour
                2 => 5, //Date
                3 => 7, //Month
            );

            echo("<br>**** Buoc tim - ".$keyword."****<br>");
            $link = "https://www.youtube.com/results?sp=".$date[$byDate]."&q=".$keyword;
            //$result = $this->getVideos($link, 'div.yt-lockup', $text);

            $result = self::getVideosByWeek($link, 'div.yt-lockup', $keyword);


            foreach ($result as $key => $item) {
               $view = $item["video_view"];
               $view = str_replace('.', '', $view);
               
               if(strlen($view) >= $arr_view[$byDate]){
                    $ins = self::insertFile($keyword, $item['video_id']);
                    if($ins)  return $item['video_id'];
               }

            }

            echo("<br>***************".date("Y-m-d H:i:s")."<br>");

        //}
        // lay title bai viet
        echo("<br>->>>>>>>>> End Crawler Videos------------<br>");

        return false;

    }

    public static function getVideosTitle($link, $att_title, $text_id){
        error_reporting(0);
        $page = array(
            "page1" => "EgIIBA%253D%253D",
            "page2" => "EgIIBEgU6gMA",
            "page3" => "EgIIBEgo6gMA",
            "page4" => "EgIIBEg86gMA",
            "page5" => "EgIIBEhQ6gMA",
            "page6" => "EgIIBEhk6gMA",
            );
        $arr_video = array();
        $string ="";
        foreach ($page as $key => $p) {
            $link = $link.$p;
            $html = file_get_html($link);
            self::$html_content = $html;

            $count = 0;
            $arr = array();

            foreach($html->find($att_title) as $e){
                $link_vids = $e->getElementsByTagName("a",1);
                $info_view = $e->getElementsById(".yt-lockup-meta-info",0);


                $view_string = $info_view->children(1);
                $view = explode(" ",$view_string->plaintext);
                if(!isset($view[1])) continue;//Next Quang cao


                $string .= "'".$link_vids->getAttribute("title")."',<br>";

            }
        }
        return $string;
    }

    public static function crawler_videos($url){
        echo '<head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="author" content="duchanh" />
        <title>Crawl</title>
        </head>';


        //$H_Crawl = new My_View_Htmldom();
        // xoa het cac the javascript va nhung the co class=".thumblock" di
        self::$arr_att_clean  = array('script','.yt-lockup');

        // lay title bai viet
        return  self::getVideos($url, 'div.yt-lockup');
    }

      // function lay title cua bai viet
    public static function getAllPlaylist($link, $att_title){
        self::$arr_att_clean  = array('script','.yt-shelf-grid-item');

        $arr = "";
        if(self::$html_content==''){
            $html = file_get_html($link);
            self::$html_content = $html;
        }else{
            $html = self::$html_content;
        }
        $count = 0;

        foreach($html->find($att_title) as $e){
            $link_vids = $e->getElementsByTagName("a",1);
            $href = "https://www.youtube.com".$link_vids->getAttribute("href");
            $arr .= "<a href='".$href."' target='_blank'>".$link_vids->plaintext."</a>";
            $view = self::getViewPlaylist($href);
            $arr .= $view."<br>";

        }
         //Load more to find Video Last just ADD -------------
            $load_more_button = $html->find(".load-more-button");

                while(count($load_more_button) > 0 && isset($load_more_button)){

                        $load_more_href = "https://www.youtube.com".$load_more_button[0]->getAttribute("data-uix-load-more-href");
                        $html_more_result  = My_Crawler_Curl::apiGET($load_more_href);
                        //html more
                        $html_more = $html_more_result->content_html;
                        $html_more = self::loadHhtml($html_more);
                        $html_more = $html_more->find(".yt-shelf-grid-item");

                        //Continue list $att_title
                        foreach($html_more as $e){
                            $link_vids = $e->getElementsByTagName("a",1);
                            $href = "https://www.youtube.com".$link_vids->getAttribute("href");
                            $arr .= "<a href='".$href."' target='_blank'>".$link_vids->plaintext."</a>";
                            $view = self::getViewPlaylist($href);
                            $arr .= $view."<br>";

                        }

                        //find widget button more
                        $btn_more  = $html_more_result->load_more_widget_html;
                        $load_more_button = self::loadHhtml($btn_more);
                        $load_more_button = $load_more_button->find(".load-more-button");

                }
            //End Load more ---------
        return $arr;
    }


    public static function getViewPlaylist($link){
            $html = file_get_html($link);
            $att = ("ul.pl-header-details");
            $ul = $html->getElementsById($att,0);
            $view = $ul->children(2);
            return $view->plaintext;
        //return $title;
    }
    public static function loadHhtml($str){
        $html_base = new simple_html_dom();
        $html_base->load($str);
        return $html_base;
    }

    public static function insertFile($file, $youtubeId){

        $youtubeId = $youtubeId.PHP_EOL;
        $file = GOOGLE_LIB."/File/".$file.".txt";
        $arr = array();

        if(is_file($file)){
            //$arr  = file_get_contents($file);
            $lines = file($file);

            //Check exits YoutubeId
            $search = array_search($youtubeId, $lines);

            if(is_numeric($search)){
                return false;
            } 


            array_unshift($lines, $youtubeId);
            file_put_contents($file, '');

            foreach ($lines as $key => $line) {
                if($key == 16) break;
                file_put_contents($file, $line, FILE_APPEND);
            }

        }else{
            file_put_contents($file, $youtubeId, FILE_APPEND);
        }
        return true;
    }

}