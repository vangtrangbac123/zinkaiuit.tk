<?php

class My_Profiler {

    static function getProfiles() {
        return array(
            'APPLICATION_ENV' => APPLICATION_ENV,
            'PROCESS_TIME'    => self::getProcessTime(),
            'MEMORY_USAGE'    => self::getMemoryUsage(),
            'CLIENT_IP'       => App::getClientIp(),
            'DATABASE'        => self::getQueryProfiles(),
            'MEMCACHE'        => My_Memcache::getProfiles(),
            'INCLUDED_FILES'  => count(get_included_files()),
        );
    }

    static function getQueryProfiles() {
        $tmp = array();
        $db = Zend_Db_Table_Abstract::getDefaultAdapter();
        if (!$db) {
            return array(
                'total_query' => 0,
                'total_sec' => 0,
                'queries' => array(),
            );
        }
        $profiler = $db->getProfiler();
        $queries = $profiler->getQueryProfiles();
        if (is_array($queries)) foreach ($queries as $query) {
            $sql = $query->getQuery();
            foreach ($query->getQueryParams() as $key => $value) {
                $value = $db->quote($value);
                if (is_int($key)) {
                    $sql = preg_replace('/\?/', $value, $sql, 1);
                } else {
                    $sql = str_replace($key, $value, $sql);
                }
            }
            $tmp[] = array(
                'sql' => $sql,
                'time' => round($query->getElapsedSecs(), 4)
            );
        }
        return array(
            'total_query' => $profiler->getTotalNumQueries(),
            'total_sec' => $profiler->getTotalElapsedSecs(),
            'queries' => $tmp,
        );
    }

    static function getMemcacheProfiles() {
        return array(
            'total_query' => 0,
            'total_sec' => 0,
            'queries' => array(),
        );
    }

    static function getProcessTime() {
        return defined('REQUEST_TIME_START') ? round((microtime(true) - REQUEST_TIME_START) * 1000, 2) . ' ms' : 0;
    }

    static function getMemoryUsage() {
        return defined('MEMORY_USAGE') ? self::toMemorySize(memory_get_usage() - MEMORY_USAGE) : 0;
    }

    static function toMemorySize($size) {
        $unit = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        return @round($size/pow(1024,($i = floor(log($size,1024)))),2) . ' ' . (isset($unit[$i]) ? $unit[$i] : 'B');
    }
}