<?php

class My_View_Admin {

    public static function multicheck($id,$data){
        foreach ($data as $key => $value) {
            if($id == $value->id) return true;
        }
        return false;
    }

    public static function replaceInFile($file, $content, $numberline){
        $lines = file($file);
        $word = $content;
        $result = '';

        foreach($lines as $line) {
            if(substr($line, 0, 1) == $numberline) {
                $result .= $word."\n";
            } else {
                $result .= $line;
            }
        }

        return file_put_contents($file, $result);
    }

    public static function changePrice($finance, $file){
        $operator_keys = rand(0, 1);
        $randomFloat = rand(0, 200) / 10;
        $random_keys = array_rand($finance, 1);

        $price_after = $finance[$random_keys]["price_after"];
        $line = $finance[$random_keys];

        if($operator_keys == 0)
            $price_after = $price_after - $price_after*$randomFloat/100;
        else
            $price_after = $price_after + $price_after*$randomFloat/100;

        $finance[$random_keys]["price_after"] = $price_after;
        $content = $line["id"]." ".$line["name"]." ".$line["price"]." ".$price_after;
        $r = My_View_Admin::replaceInFile($file, $content, $line["id"]);

        if($r)
        return $finance;
        else
        return false;
    }

    public static function isActive($data, $field = 'is_active') {
        if (!isset($data->{$field})) return '';
        $checked = (int)$data->{$field} == 1 ? 'checked="checked"' : '';
        return '<div class="switch switch-sm switch-primary"><input type="checkbox" data-plugin-ios-switch ' . $checked . ' /></div>';
    }

    public static function isVoice($data, $field = 'is_voice') {
        if (!isset($data->{$field})) return '';
        $checked = (int)$data->{$field} == 1 ? 'checked="checked"' : '';
        return '<div class="switch switch-sm switch-primary"><input type="checkbox" data-plugin-ios-switch ' . $checked . ' /></div>';
    }

    public static function notificationStatus($statusId) {
        switch ($statusId) {
            case 0:
                return '<span class="label label-primary">New</span>';

            case 1:
                return '<span class="label label-success">Success</span>';

            case 2:
                return '<span class="label label-danger">Fail</span>';

            case 10:
                return '<span class="label label-info">In Process</span>';
        }
    }

    public static function agodaCouponStatus($statusId) {
        switch ($statusId) {
            case 1:
                return '<span class="label label-info">New</span>';

            case 2:
                return '<span class="label label-primary">Received</span>';

            case 3:
                return '<span class="label label-success">Used</span>';

            default:
                return '<span class="label label-info">Undefined</span>';
        }
    }
}