<?php
class My_Api{
	protected static $_instance;
	public static function getInstance() {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public static function request($link, $data, $method){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 300);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        //curl_setopt($curl, CURLOPT_COOKIEJAR, COOKIEFILE);
        //curl_setopt($curl, CURLOPT_COOKIEFILE, COOKIEFILE);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($curl, CURLOPT_TIMEOUT, 120);

        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
        } else {
            $link = self::mergeQueryString($link, $data);
        }
        curl_setopt($curl, CURLOPT_URL, $link);
        $result = curl_exec($curl);
        return  json_decode($result);
        // if (!$result) {
        //     throw new Exception('Connection error: ' . $error);
        // }

    }



    public static function mergeQueryString($url, $params = null) {
        return $url . (empty($params) ? '' : (parse_url($url, PHP_URL_QUERY) === null ? '?' : '&') . http_build_query($params));
    }

}