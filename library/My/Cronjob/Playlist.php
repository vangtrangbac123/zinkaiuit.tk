<?php

class My_Cronjob_Playlist {
	public static function takecarePlaylist(){
		 $list = My_Model_Helperweb::getInstance()->Google->getListCare();
		 if(empty($list)) return;

		 //Start Service
		 foreach ($list as $key => $channel) {
		 	//GET VIDEO ID

		 	$log = array(
		 		'auth_google_id' => $channel->auth_google_id,
		 		'channel' => $channel->channel_name,
		 		'keyword' => $channel->keyword,
		 		'message' => "Không Có Video Để Add",
		 		'status'  => 0,
		 		'video_id' => "",
		 		'date_add' => date("Y-m-d H:i:s"),);


        	 $videoId = My_Crawler_Htmldom::getVideoToAddPlaylist($channel->keyword);
        	 if($videoId){
        	 	 //Start Service
        	 	$log['video_id'] = $videoId;
        	 	 $init = new  Google_Init();
		         $token = $init->updateExpiredToken($channel->auth_google_id);
		         if(!$token){
		         	//Log
		         	$log['message'] = "Không thể kết nối đến Channel này (Có thể DIE)";
		         	My_Model_Helperweb::getInstance()->Crawlerlog->save($log);
		         	continue;
		         }
		         $service = new Google_Service_YouTube($init->client);

		         //GET LIST PLAYLIST
		         $list_playlist = self::getPlaylistByChannel($service);
		         if(!is_array($list_playlist)){
		         	//Log
		         	$log['message'] = "Không Có Playlist Nào Trong Channel";
		         	My_Model_Helperweb::getInstance()->Crawlerlog->save($log);
		         	continue;
		         }


		        //Add video to playlists
		        foreach ($list_playlist as $key => $playlist) {
		        	$response = Google_Api::addVideo($service, $playlist["id"], $videoId, $channel->add_vids_position);
		        }

		        if($response){
		        		        	//Log
		         	$log['message'] = "Thêm Video Thành công";
		         	$log['status']  = 1;
		         	My_Model_Helperweb::getInstance()->Crawlerlog->save($log);
	         	}

		        

        	 }else{ My_Model_Helperweb::getInstance()->Crawlerlog->save($log);}
        	 
		 }

		 echo("<br>***************".date("Y-m-d H:i:s")."<br>");
		 echo("<br>->>>>>>>>> End Add Videos------------<br>");
         

         

         //Do
   //       $item = Google_Api::addVideo($service, $playlistId, $videoId, $position = false);
		 // var_dump($list);die;

		//My_Crawler_Htmldom::getVideoToAddPlaylist("Mickey Mouse");die;
	}

	public static function getPlaylistByChannel($service){
		 //By Myself
        $mine = Google_Api::playlistsListByChannelId($service,
        'snippet,contentDetails', 
        array('mine' => true, 'maxResults' => 25, 'onBehalfOfContentOwner' => '', 'onBehalfOfContentOwnerChannel' => ''));
        //Total Playlist
        $total = $mine->pageInfo->totalResults;
        $page = CEIL($total/25);
        $nextPage = $mine->nextPageToken;

        $list_playlist = array();
        //var_dump($mine->items[0]);die;
        foreach ($mine->items as $key => $playlist) {
               $list_playlist[] = array(
                    'id' => $playlist->id,
                    'title' => $playlist->snippet->title,
                    'description' => $playlist->snippet->description,
                    'item_count'  => $playlist->contentDetails->itemCount

               );
            } 
         //Pagination
        for($i = 1; $i < $page;$i++){

        	$next = Google_Api::playlistsListByChannelId($service,
			        'snippet,contentDetails', 
			        array('mine' => true, 'maxResults' => 25,'onBehalfOfContentOwner' => '', 'onBehalfOfContentOwnerChannel' => '', 'pageToken' => $nextPage));

        	 //var_dump($mine->items[0]);die;
	        foreach ($next->items as $key => $playlist) {
	               $list_playlist[] = array(
	                    'id' => $playlist->id,
	                    'title' => $playlist->snippet->title,
	                    'description' => $playlist->snippet->description,
	                    'item_count'  => $playlist->contentDetails->itemCount

	               );
	            }
	         $nextPage = $next->nextPageToken;

		}
 
         return $list_playlist;
	}
}
