<?php
require APPLICATION_PATH . '/modules/web/controllers/ajax/simple_html_dom.php';
class My_Cronjob_View extends My_Controller_Action{
	public $html_content = '';
    public $arr_att_clean = array();

	public static function getPlaylistView(){
		//$link = 'https://www.youtube.com/results?q=dr+phil+september+20+2016&sp=EgQIAxAB';
        $arr_text = My_Model_Helperweb::getInstance()->Google->getListCare();
        echo("<br>-----------Begin Crawler Playlist------------ <br>");
        //check data
        if(count($arr_text) == 0){
                echo("<br>********No data to Add ******* <br>");
                die;
        }

        
        foreach ($arr_text as $key => $value) {
            $arr_playlist = array();
            $arr_playlist["channel"] = $value->channel_id;
            $arr_playlist["channel_name"] = $value->channel_name;
            $arr_playlist["text_value"] = $value->keyword;
            $arr_playlist["is_active"] = 1;
            $arr_playlist["date_add"] = date("Y-m-d H:i:s");

             echo("<br>-----------Playlist".$arr_playlist["text_value"]." - ".$arr_playlist["channel_name"]."  ------------ <br>");


            $link = "https://www.youtube.com/channel/".$value->channel_id."/playlists";

            // lay title bai viet
             $html = file_get_html($link);
              foreach($html->find('li.yt-shelf-grid-item') as $e){
                    $link_vids = $e->getElementsByTagName("a",1);
                    $href = "https://www.youtube.com".$link_vids->getAttribute("href");
                    //$arr_playlist["playlist_title"] =  preg_replace("/^'|[^A-Za-z0-9\'-]|'$/", '', $link_vids->plaintext);
                    $arr_playlist["playlist_title"] =  $link_vids->plaintext;
                    $arr_playlist["playlist_view"]  = self::getViewPlaylist($href);
                    $arr_playlist["playlist_id"]    = substr($link_vids->getAttribute("href"),15);
                    if(isset($arr_playlist["playlist_id"])){
                         My_Model_Helperweb::getInstance()->Cronjobview->insertPlaylistView($arr_playlist);
                    }

                }
                 //Load more to find Video Last just ADD -------------
                $load_more_button = $html->find(".load-more-button");

                while(count($load_more_button) > 0 && isset($load_more_button)){

                        $load_more_href = "https://www.youtube.com".$load_more_button[0]->getAttribute("data-uix-load-more-href");
                        $html_more_result  = My_Api::request($load_more_href,array(),"GET");
                        //html more
                        $html_more = $html_more_result->content_html;
                        $html_more = self::loadHhtml($html_more);
                        $html_more = $html_more->find(".yt-shelf-grid-item");

                        //Continue list $att_title
                        foreach($html_more as $e){
                            $link_vids = $e->getElementsByTagName("a",1);
                            $href = "https://www.youtube.com".$link_vids->getAttribute("href");
                            //$arr_playlist["playlist_title"] =  preg_replace("/^'|[^A-Za-z0-9\'-]|'$/", '', $link_vids->plaintext);
                            $arr_playlist["playlist_title"] =  $link_vids->plaintext;
                            $arr_playlist["playlist_view"]  = self::getViewPlaylist($href);
                            $arr_playlist["playlist_id"]    = substr($link_vids->getAttribute("href"),15);
                            if(isset($arr_playlist["playlist_id"])){
                                  My_Model_Helperweb::getInstance()->Cronjobview->insertPlaylistView($arr_playlist);
                            }

                        }
  
                        //find widget button more
                        $btn_more  = $html_more_result->load_more_widget_html;
                        $load_more_button = self::loadHhtml($btn_more);
                        $load_more_button = $load_more_button->find(".load-more-button");


                }
            //End Load more ---------


        }

		// lay title bai viet
        echo("<br>->>>>>>>>> End Crawler ------------<br>");
	}



    public  static function getViewPlaylist($link){
            $html = file_get_html($link);
            // $att = "ul.pl-header-details";
            //$ul = $html->getElementsById("ul.pl-header-details",0);
            //$obj = $html->getElementsById("ul.pl-header-details",0);
            if ($obj = $html->getElementsById("ul.pl-header-details",0)) {
                $view = $obj->children(2);
                return preg_replace('/[^0-9]+/', '', $view->plaintext);
            } else {
                return -1;
            }
            
        //return $title;
    }
    public  static function loadHhtml($str){
        $html_base = new simple_html_dom();
        $html_base->load($str);
        return $html_base;
    }
	
}