<?php

class My_Controller_Action extends Zend_Controller_Action {

    public $auth;
    public $user;
    public $model;

    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        $this->setRequest($request)
             ->setResponse($response)
             ->_setInvokeArgs($invokeArgs);
        $this->_helper = new Zend_Controller_Action_HelperBroker($this);

        $this->auth = Zend_Auth::getInstance();
        $this->user = $this->auth->getIdentity();
        $this->model = My_Model_Helperweb::getInstance();
        $this->moduleName     = $this->_request->getModuleName();
        $this->actionName     = $this->_request->getActionName();
        $this->controllerName = $this->_request->getControllerName();
        $this->client         = new Google_Client();

        $this->init();
    }

    public function postDispatch()
    {
        $this->view->model          = $this->model;
        $this->view->auth           = $this->auth;
        $this->view->user           = $this->user;
        $this->view->head           = new My_View_Head($this->view);
        $this->view->requestUri     = urlencode($_SERVER['REQUEST_URI']);
        $this->view->moduleName     = $this->moduleName;
        $this->view->actionName     = $this->actionName;
        $this->view->controllerName = $this->controllerName;
        $this->view->client         = $this->client;
        $this->_postDispatch();
    }

    public function _postDispatch() {}
}