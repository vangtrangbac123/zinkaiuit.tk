<?php

class My_Controller_Form extends My_Controller_Action {

    const ROWS_PER_PAGE = 50;

    public function onSave($form, $data, $post) {
        return $this->model->{$form['model']}->save($data);
    }

    public function onSaveBefore($data, $post) {
        return $data;
    }

    public function onSaveAfter($id, $data) {
        return $data;
    }

    public function onSaveDone($id, $data, $post) {
        return $this->_redirect(sprintf('/admin/%s/%s/id/%d/?success=1', $post['_controller'], $post['_action'], $id));
    }

    public function init() {

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if(!isset($post['_form'])) return;
            $form = Admin_Model_Form::get($post['_form']);
            $data = array();
            if (isset($post[$form['primary']])) {
                $data[$form['primary']] = $post[$form['primary']];
            }
            foreach ($form['fields'] as $key => $field) {
                if (substr($key, 0, 1) == '@') continue;
                if ($field['type'] == Admin_Model_Form::TYPE_CHECKBOX) {
                    $data[$key] = isset($post[$key]) ? 1 : 0;
                } else {
                    $data[$key] = isset($post[$key]) ? $post[$key] : null;
                }
            }
            $data = $this->updateDateAdd($form, $data, $post);
            $data = $this->onSaveBefore($data, $post);
            $id = $this->onSave($form, $data, $post);
            $this->onSaveAfter($id, $data);
            $this->onSaveDone($id, $data, $post);
        }
    }

    private function updateDateAdd($form, $data, $post) {
        if (isset($form['primary'])
            && isset($data[$form['primary']])
            && empty($data[$form['primary']])) {
            $list = array('Banner', 'Download', 'News', 'Category','Gallery');
            if (in_array($form['model'], $list)) {
                $data['date_update'] = $data['date_add'] = date('Y-m-d H:i:s');
            }
        }
        return $data;
    }

    public function paginateList($data) {
        $sort = (isset($data['sort']) && !empty($data['sort'])) ? $data['sort'] : null;
        $url = sprintf('/admin/%s/%s/page/', $this->controllerName, $this->actionName) . '%d/';
        // if(!empty($sort)) {
        //     $url = sprintf('/%s/%s/page/', $this->controllerName, $this->actionName) . '%d/?' . http_build_query($sort);
        // }

        $page = $this->_getParam('page', 1);
        $this->view->form = Admin_Model_Form::get($this->_form);
        $this->view->list = isset($data['rows']) ? $data['rows'] : $data;
        $this->view->sort = $sort;
        if (isset($data['total'])) {
            $this->view->pagination = Utility_Paginator::create($url, $page, $data['count'], $data['total']);
        }
    }

    public function indexAction() {
        $data = $this->getList();
        $this->paginateList($data);
    }

    public function detailAction() {
        $id = (int)$this->_getParam('id', 0);
        if ($id > 0) {
            $this->view->data = $this->getDetail($id);
        }
        $this->view->form = Admin_Model_Form::get($this->_form);
    }

    public function getList() {
        return $this->model->{$this->_form}->selectAll();
    }

    public function getDetail($id) {
        return $this->model->{$this->_form}->get($id);
    }

    public function getListAutoPaging($sql, $bind = null) {
        $page = abs((int)$this->_getParam('page', 1));
        $count = abs((int)$this->_getParam('count', self::ROWS_PER_PAGE));
        $offset = ($page - 1) * $count;
        $sql .= " LIMIT $offset, $count";
        $data = $this->model->Location->getRowsAndTotal($sql, $bind);
        if (!$data) return $data;
        $data['count'] = $count;
        return $data;
    }
}