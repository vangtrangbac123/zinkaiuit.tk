 <?php

class Utility_Validate
{
    public static function isValid($what, $data)
    {
    	switch($what) {

            case 'name':
                $tmp = explode(' ', $data);
                return count($tmp) > 1;
                break;

    		// validate a phone number
    		case 'phone':
                $pattern = "/^[0-9]{9,11}$/i";

                // wrong pattern???
    			// $pattern = "/^([1]-)?[0-9]{3}-[0-9]{3}-[0-9]{4}$/i";
    			break;

            case 'mobile':
                return self::checkMobileNumber($data);
                break;

    			// validate email address
    		case 'email':
    			$pattern = "/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/i";
    			break;

    		default:
    			return false;
    			break;
    	}

    	return preg_match($pattern, $data) ? true : false;
    }

    public static function checkMobileNumber($number)
    {
        $smsConf = array(
            '120','121','122','126','128','129',
            '902','903','904','905','906','907','908','909',
            '932','933','934','935','936','937','938','939',
            '123','124','125','127','129',
            '912','913','914','915','916','917','918','919',
            '942','943','944','945','946','947','948','949',
            '163','164','165','166','167','168','169',
            '972','973','974','975','976','977','978','979',
            '982','983','984','985','986','987','988','989',
            '962','963','964','965','966','967','968','969'
        );

        if (substr($number, 0, 2) == 84)
            $number = '0' . substr($number, 2);

        if (!empty($number)) {
            if (substr($number, 0, 1) == 0) {
                $number = substr($number,1);
            }
        }

        if (strlen($number) > 11 || strlen($number) < 9)
            return false;

        $arr = str_split($number);

        foreach ($arr as $chr) {
            if (ord($chr) < 32 || ord($chr) > 126) return false;
        }

        return in_array(substr($number, 0, 3), $smsConf);
    }

    public static function trimEmail($email){
        $test = substr($email, 0, strpos($email,"@"));
        if($test == ""){
            $test = $email;
        }
        return $test;
    }

}