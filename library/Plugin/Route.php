<?php

class Plugin_Route extends Zend_Controller_Plugin_Abstract
{
    // called before Zend_Controller_Front calls on the router to evaluate the request against the registered routes.
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
    }

    // is called after the router finishes routing the request.
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
    }

    // is called before an action is dispatched by the dispatcher
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if ($request->getModuleName() == 'admin' && $request->getControllerName() != 'auth') {
            $auth = Zend_Auth::getInstance();
            if (!$auth->hasIdentity()) {
                    $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
                    $redirector->gotoUrlAndExit('/admin/auth');
            }
        }
    }

    // is called after an action is dispatched by the dispatcher. This callback allows for proxy or filter behavior
    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
    }

    // is called before Zend_Controller_Front enters its dispatch loop.
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
    }

    // is called after Zend_Controller_Front exits its dispatch loop.
    public function dispatchLoopShutdown()
    {
    }
}
